<div class="col-md-4 col-sm-4 bg-white">
    <?php

        $temp = 'Прогноз недоступен';
        $pic = '';
        $type = '';

        try
        {
            $city_id = 35394; // id города
            
            $data_file="http://export.yandex.ru/weather-ng/forecasts/$city_id.xml"; // адрес xml файла 

            $xml = simplexml_load_file($data_file); // раскладываем xml на массив

            $temp = $xml->fact->temperature;

            $pic = $xml->fact->image;

            $type = $xml->fact->weather_type;
        }
        catch (Exception $e) {}

    ?>

    <p class="text-right weat">
        <a href="https://pogoda.yandex.kz/karaganda/" target="_blank">
            Караганда
        </a>: 
        <img src="http://img.yandex.net/i/wiz{{ $pic }}.png" alt="{{ $type }}" title="{{ $type }}">
        {{ $temp }} <sup>o</sup>C,
        <small>{{ $type }}</small>
    </p>

    <!-- start of weather widget -->
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Forex Rates Ticker Script - EXCHANGERATEWIDGET.COM -->

        <!-- End of Exchange Rates Script -->
    <!-- end of currency widget -->
</div>

