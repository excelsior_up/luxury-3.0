
   <footer class="clearfix  col-xs-12 ">
    <div class="">
      <div class="col-lg-12 col-md-12 col-xs-12" >
            <h3 class="text-center">"Luxury Life" &copy; 2015</h3>
      </div>
      <div class="row">
        <section class="col-lg-4 col-md-4 col-xs-12 footsection ">
          <h3 >Страницы</h3>
            <ul>
                <li class="col-xs-12"><a href="/">Главная</a></li>
                <li class="col-xs-12"><a href="/#2p">О нас</a></li>
                <li class="col-xs-12"><a href="/rules-agreed">Регистрация</a></li>
                <li class="col-xs-12"><a href="/signin">Войти</a></li>
            </ul>
        </section>
        <section class="col-lg-4 col-md-4 col-xs-12 footsection" style="margin-bottom:15px">
          <h3> Контакты</h3>
          <ul>
            <li class="col-xs-12"><i class="fa fa-phone"></i> <a href="tel:+7 778 109 02 94">+7 778 109 02 94</a> </li>
            <li class="col-xs-12"><i class="fa fa-phone"></i> <a href="tel:+7 777 427 64 15">+7 777 427 64 15</a> </li>
            <li class="col-xs-12"><i class="fa fa-envelope"></i> <a href="mailto:info@luxurylife.us">info@luxurylife.us</a> </li>
          </ul>
        </section>
        <section class="col-lg-4 col-md-4 col-xs-12 footsection">
          <h3>Адрес</h3>
          <ul>
            <li  style="white-space: nowrap;"><i class="fa fa-map-marker"></i> Республика Казахстан,город Караганда</li>
            <li><i class="fa fa-location-arrow"></i>  ул. Н.Абдирова, 43 </li>
          </ul>
        </section>
      </div>

      <div class="row">
        <div class="col-lg-12 col-md-12 sm-center" style="margin-left:30px; font-size: 18px">
            <p align="center"></p>

              <a id="madeBy" href="http://excelsior.su" target="_blank"  style="width: 50px">
                <img src="/landing_resources/img/excelsior.png" class="excelsior img-responsive">
                Excelsior Services
              </a>
        </div>
      </div>
    </div>

  </footer>