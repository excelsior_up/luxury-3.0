
  <div class="tree" >  
        @if($sponsor)
          <h2>Ваш спонсор: {{ $sponsor->name . " " . $sponsor->surname}}</h2>                                          
        @endif
        <ul >
            <li>
                <div style="font-size:20px;" >
                 {{ floor($structure->children->count()/12) }} <i class="fa fa-eur" ></i>
                </div>
                <a href="#" data-toggle="tooltip" data-placement="left" title="{{$structure->name .' '.$structure->surname}}"><img src="{{$structure->avatar}}" alt="" class="img-mini"></a>
                  <?php $counter = 0; ?>
                  @foreach ($structure->children as $key => $child)
                      @if($key % 12 == 0)
                         <ul>
                          <?php $check= $key;?>
                      @endif
                        <li>
                          <a href="#"  data-toggle="tooltip" data-placement="bottom" title="{{$child->name.' '.$child->surname}}"><img src="{{$child->avatar}}" alt="" class="img-mini"></a>
                        </li>
                      @if($key - $check == 11)
                          </ul>
                      @endif
                      <?php $counter = $key+1; ?>
                  @endforeach

                  @if(!$structure->children->count())
                    <ul>
                  @endif
                  @for ($i= $counter%12; $i < 12; $i++) 
                    <li>
                      <a href="#"  data-toggle="tooltip" data-placement="bottom" title="отсутствует"><img src="" alt="" class="img-mini def-circle"></a>

                    </li>
                    
                  @endfor
                  @if($counter%12 != 0)
                    </ul >
                  @endif
            </li>
        </ul>
    </div>

{{--  @foreach ($structure->children as $key => $child)
                      <li class="a{{ $key+1 }}">
                          <a href="javascript:showSubscriber({{$child->id}})" data-toggle="tooltip" data-placement="left" title="{{$child->name.' '.$child->surname}}"><img src="{{$child->avatar}}" alt="" class="img-mini"></a>
                          <ul>
                               @foreach ($child->children as $key1 => $child_1)
                                   
                                  <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="{{$child_1->name.' '.$child_1->surname}}"><img src="{{$child_1->avatar}}" alt="" class="img-mini"></a></li>
                                  
                               @endforeach
                               @if ($child->children->count() < 3)
                                   @for ($i = 0; $i < 3-$child->children->count(); $i++)
                                       <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a></li>
                                   @endfor      
                               @endif
                          </ul>
                      </li>
                   @endforeach
                   @if ($structure->children->count() < 3)
                       @for ($i = 0; $i < 3-$structure->children->count(); $i++)
                           <li class="a{{ $i + 1 + $structure->children->count() }}">
                              <a href="#" data-toggle="tooltip" data-placement="left" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a>
                              <ul>
                                   @for ($j = 0; $j < 3; $j++)
                                       <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a></li>
                                   @endfor     
                              </ul>
                           </li>
                       @endfor      
                   @endif --}}


     {{-- <li class="a2">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                        <ul>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                            <li class="b1">
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                            </li>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                        </ul>
                    </li>
                    
                    
                     <li class="a3">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                         <ul>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                            <li class="b1">
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                            </li>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван
                                erewr"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                        </ul>
                    </li> --}}