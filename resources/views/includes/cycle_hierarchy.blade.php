
    <div class="tree">                                            
        <ul>
            <li>
                <a href="#" data-toggle="tooltip" data-placement="left" title="{{$structure_cycle->user->name .' '.$structure_cycle->user->surname}}"><img src="{{$structure_cycle->user->avatar}}" alt="" class="img-mini"></a>
                <ul>
                    @foreach ($structure_cycle->children as $key => $child)
                       <li class="a{{ $key+1 }}">
                           <a href="javascript:showSubscriber({{$child->user->id}},{{$structure_cycle->cycle}})" data-toggle="tooltip" data-placement="left" title="{{$child->user->name.' '.$child->user->surname}}"><img src="{{$child->user->avatar}}" alt="" class="img-mini"></a>
                           <ul>
                                @foreach ($child->children as $key1 => $child_1)
                                   <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="{{$child_1->user->name.' '.$child_1->user->surname}}"><img src="{{$child_1->user->avatar}}" alt="" class="img-mini"></a></li>
                                   
                                @endforeach
                                @if ($child->children->count() < 3)
                                    @for ($i = 0; $i < 3-$child->children->count(); $i++)
                                        <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a></li>
                                    @endfor      
                                @endif
                           </ul>
                       </li>
                    @endforeach
                    @if ($structure_cycle->children->count() < 3)
                        @for ($i = 0; $i < 3-$structure_cycle->children->count(); $i++)
                            <li class="a{{ $i + 1 + $structure_cycle->children->count() }}">
                               <a href="#" data-toggle="tooltip" data-placement="left" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a>
                               <ul>
                                    @for ($j = 0; $j < 3; $j++)
                                        <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a></li>
                                    @endfor     
                               </ul>
                            </li>
                        @endfor      
                    @endif

                </ul>
            </li>
        </ul>
    </div>

    
    <div class="tree">                                            
        <ul>
            <li>
                <a href="#" data-toggle="tooltip" data-placement="left" title="{{$structure->name .' '.$structure->surname}}"><img src="{{$structure->avatar}}" alt="" class="img-mini"></a>
                <ul>
                    @foreach ($structure->children as $key => $child)
                      <li class="a{{ $key+1 }}">
                          <a href="javascript:showSubscriber({{$child->id}})" data-toggle="tooltip" data-placement="left" title="{{$child->name.' '.$child->surname}}"><img src="{{$child->avatar}}" alt="" class="img-mini"></a>
                          <ul>
                               @foreach ($child->children as $key1 => $child_1)
                                   
                                  <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="{{$child_1->name.' '.$child_1->surname}}"><img src="{{$child_1->avatar}}" alt="" class="img-mini"></a></li>
                                  
                               @endforeach
                               @if ($child->children->count() < 3)
                                   @for ($i = 0; $i < 3-$child->children->count(); $i++)
                                       <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a></li>
                                   @endfor      
                               @endif
                          </ul>
                      </li>
                   @endforeach
                   @if ($structure->children->count() < 3)
                       @for ($i = 0; $i < 3-$structure->children->count(); $i++)
                           <li class="a{{ $i + 1 + $structure->children->count() }}">
                              <a href="#" data-toggle="tooltip" data-placement="left" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a>
                              <ul>
                                   @for ($j = 0; $j < 3; $j++)
                                       <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Отсутсвует"><img src="" alt="" class="img-mini def-circle"></a></li>
                                   @endfor     
                              </ul>
                           </li>
                       @endfor      
                   @endif

                  
                    <!-- <li class="a1">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Иванов Иван
                        123"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                         <ul>
                            <li class="b1"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                            <li class="b1">
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                            </li>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                        </ul>
                    </li>
                    
                    
                    <li class="a2">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                        <ul>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                            <li class="b1">
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                            </li>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                        </ul>
                    </li>
                    
                    
                     <li class="a3">
                        <a href="#" data-toggle="tooltip" data-placement="left" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                         <ul>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                            <li class="b1">
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван"><img src="/images/img.jpg" alt="" class="img-mini"></a>
                            </li>
                            <li class="b1"><a href="#" data-toggle="tooltip" data-placement="bottom" title="Иванов Иван
                                erewr"><img src="/images/img.jpg" alt="" class="img-mini"></a></li>
                        </ul>
                    </li> -->
                </ul>
            </li>
        </ul>
    </div>