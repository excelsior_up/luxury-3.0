<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.ico">

    <title>Luxury Life</title>



    <!-- Bootstrap core CSS -->

    <link href="/god_files/css/bootstrap.min.css" rel="stylesheet">
    <link href="/god_files/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="/god_files/css/animate.min.css" rel="stylesheet">

    <!-- <link href="/god_files/css/roboto.min.css" rel="stylesheet"> -->
    <link href="/god_files/css/material-fullpalette.min.css" rel="stylesheet">
    <link href="/god_files/css/ripples.min.css" rel="stylesheet">
    <link href="/god_files/css/snackbar.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="/god_files/css/custom.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="/god_files/css/maps/jquery-jvectormap-2.0.1.css" /> -->
    <link href="/god_files/css/icheck/flat/green.css" rel="stylesheet" />
    <link href="/god_files/css/floatexamples.css" rel="stylesheet" type="text/css" />
    <link href="/god_files/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


    <!-- Include Editor style. -->
    <link href="/god_files/css/froala_editor.min.css" rel="stylesheet" type="text/css" />
    <link href="/god_files/css/froala_style.min.css" rel="stylesheet" type="text/css" />
    <link href="/god_files/css/vendor/calendar/calendar.css">

    <script src="http://code.jquery.com/jquery-1.11.0.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="/god_files/js/vendor/jquery.browser.min.js"></script>

    <script src="/god_files/js/calen/jquery-ui-datepicker.min.js"></script>

    <!-- <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script> -->

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    @include('includes.analytics')

    <div class="container body">


        <div class="main_container">

            @include('admin.menu')

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="dashboard_graph">
                            @yield("content")
                        </div>
                    </div>
                </div>
                <!-- footer content -->
                <footer>
                    <div class="">
                        <p align="center">Все права защищены 2015
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
            <!-- /page content -->

        </div>

    </div>

    <script src="/god_files/js/froala_editor.min.js"></script>
    <!-- skycons -->

    <script src="/god_files/js/skycons/skycons.js"></script>
    <script src="/god_files/js/ripples.min.js"></script>
    <script src="/god_files/js/material.min.js"></script>
    <script src="/god_files/js/snackbar.min.js"></script>
    <script>
        var icons = new Skycons({
                "color": "#73879C"
            }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);

        icons.play();
    </script>
     <script>
      $(function() {
          $('#edit').editable({inlineMode: false})
      });

    </script>
    <!-- /footer content -->
    <script src="/god_files/js/bootstrap.min.js"></script>
    <script src="/god_files/js/custom.js"></script>

    <script>
        $(document).ready(function() {

            // This command is used to initialize some elements and make them work properly
            $.material.init();
            $('#calendar').datepicker({
                inline: true,
                firstDay: 1,
                showOtherMonths: true,
                dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            });

        });


    </script>

</body>

</html>
