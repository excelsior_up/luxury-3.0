@extends('registr')

@section('content')
    <div class="row x_title">
        <div class="col-md-6">
            <h3>Редактировать профиль</h3>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 ">

    @if(Session::has('edit_message'))
        <p style="color:red; font-size:17px;">{{ Session::get('edit_message') }}</p>
    @endif
    {!! Form::open(['url' => '/club/edit-profile', 'method' => 'post','class' => 'form-horizontal form-label-left']) !!}
        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Фамилия <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" name="surname" value="{{ $user->surname }}" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Имя <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" name="name" value="{{ $user->name }}" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Отчество <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" name="patronymic" value="{{ $user->patronymic }}" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>


        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Email <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="name" name="email" value="{{ $user->email }}" class="form-control col-md-7 col-xs-12"  required="required" type="email">
                </div>
        </div>

        @if(Session::has('message'))
            <p style="color:red; font-size:20px;">{{ Session::get('message') }}</p>
        @endif


        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Телефон <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  name="phone" value="{{ $user->phone }}" data-inputmask="'mask' : '+7(999) 999-99-99'" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Адрес <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  name="address" value="{{ $user->address }}" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Дата рождения <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        {{-- Calendar disabled --}}
                        <input  name="birth_date"
                        value="{{ date('d.m.Y', strtotime($user->birth_date)) }}"
                        data-inputmask="'mask': '99.99.9999'"
                        class="form-control col-md-7 col-xs-12 "
                        required="required"
                        type="text"
                        placeholder="дд.мм.гггг">
                </div>
        </div>
         <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Банковский счет <span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  name="bank_account" value="{{ $user->bank_account }}" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">ИИН<span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  name="bik" value="{{ $user->bik }}" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>



        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Название банка<span class="required">*</span>
            </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                        <input  name="bank_name" value="{{ $user->bank_name }}" class="form-control col-md-7 col-xs-12"  required="required" type="text">
                </div>
        </div>

        <button class="btn btn-primary btn-raised">Сохранить<div class="ripple-wrapper"></div></button>
    {!! Form::close() !!}
    <button class="btn btn-primary btn-raised" data-toggle="modal" data-target="#complete-dialog">Изменить пароль<div class="ripple-wrapper"></div></button>
    </div>
    <div id="complete-dialog" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" >&times;</button>

            </div>
            {!! Form::open(['url' => '/club/change-password', 'method' => 'post','class' => 'form-horizontal form-label-left']) !!}
                <div class="modal-body">
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Старый пароль <span class="required"></span>
                         </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="name" name="old_password"  class="form-control col-md-7 col-xs-12"   type="password" required>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Новый пароль</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="pass" type="password" name="new_password" data-validate-length-range="6" class="form-control col-md-7 col-xs-12" required="required" >
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Новый пароль еще раз</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                             <input id="pass2" type="password" name="new_password2" data-validate-linked="new_password" class="form-control col-md-7 col-xs-12" required="required" >
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">Отменить</button>
                      <button type="submit" class="btn btn-primary" id="but-save" disabled>Сохранить</button>
                </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>

    @include('includes.right')

    <div class="clearfix"></div>


@stop
