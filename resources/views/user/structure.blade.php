@extends('app')

@section('content')
    <div class="row x_title">
        <div class="col-md-6">
            <h3>��� ���������</h3>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_content">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">1-� ����</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">2-� ����</a>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <?php $structure = $user; ?>
                        @include('includes.hierarchy')
                    </div>
                    
                    @for($i = 2; $i <= 5; $i++)
                        <div role="tabpanel" class="tab-pane fade" id="tab_content{{$i}}" aria-labelledby="home-tab">
                            <div class="tree">
                                @if($sponsor)
                                    <h2>��� �������: {{ $sponsor->name . " " . $sponsor->surname}}</h2>
                                @endif
                                <ul>
                                    <li>
                                        <div style="font-size:20px;">
                                            {{ floor($structure->childrenEuroCount($i)->count()/12) }} <i class="fa fa-eur" ></i>
                                        </div>
                                        @if($structure->cycle >= $i)
                                            <a href="#" data-toggle="tooltip" data-placement="left" title="{{$structure->name .' '.$structure->surname}}"><img src="{{$structure->avatar}}" alt="" class="img-mini"></a>
                                        @else
                                            <a href="#"  data-toggle="tooltip" data-placement="bottom" title="�� �� ������� �� ���� �������"><img src="" alt="" class="img-mini def-circle"></a>
                                        @endif
                                        <?php
                                        $check = 0;
                                        $counter = 0;
                                        ?>
                                        @foreach ($cycles as $key => $c)
                                            @if($c->cycle >= $i)
                                                @if($key % 12 == 0)
                                                    <ul>
                                                        <?php $check= $counter;?>
                                                        @endif
                                                        <li>
                                                            <a href="#"  data-toggle="tooltip" data-placement="" title="{{$c->name.' '.$c->surname}}"><img src="{{$c->avatar}}" alt="" class="img-mini"></a>
                                                        </li>


                                                        @if($counter - $check == 11)
                                                    </ul>
                                                @endif
                                                <?php $counter++; ?>
                                            @endif

                                        @endforeach

                                        @if(!$cycles->count())
                                            <ul>
                                                @endif
                                                @if($counter == 0 or $counter % 12 != 0)
                                                    @for ($iii= $counter % 12; $iii < 12; $iii++)
                                                        <li>
                                                            <a href="#"  data-toggle="tooltip" data-placement="bottom" title="O����������"><img src="" alt="" class="img-mini def-circle"></a>
                                                        </li>
                                                    @endfor
                                                @endif
                                                @if($counter % 12 != 0 or !$cycles->count())
                                            </ul >
                                        @endif

                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endfor


                </div>

                {{--  --}}


            </div>
        </div>
    </div>
    </div>

    <div id="subscriber">

    </div>


    <script>
        function showSubscriber(id, cycle_number)
        {
            $.ajax({
                url: "/user/one-structure",
                type: "GET",
                dataType: "text",
                data: {id:id, cycle_number:cycle_number},
                success: function(result) {
                    //alert(result+" fd");
                    $("#subscriber").html(result);
                },
                error:function(result){
                    console.log(result);
                }
            });
        }


    </script>

@stop
