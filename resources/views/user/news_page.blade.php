@extends('registr')

@section('content')

<div class="row x_title">
    <div class="col-md-6">
        <h3>{!! $post->title !!}</h3>
    </div>
</div>

<div class="col-md-8 col-sm-8 ">
   	<img src="{{$post->image}}" class="img-responsive news-pic">
   	<p class="text-market">
   		{!! $post->post !!} 
    </p>
</div>

<div class="clearfix"></div>

@stop