<div class="panel-group" id="2accordion{{ $user->id }}" role="tablist" aria-multiselectable="true">

    @foreach($user->children as $child)

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{ $child->id }}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#2accordion" href="#2collapse{{ $child->id }}" aria-expanded="true" aria-controls="2collapse{{ $child->id }}">
                        {{ $child->position }}. {{ $child->name }} - {{ $child->login }}
                    </a>
                </h4>
            </div>

            <div id="2collapse{{ $child->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $child->id }}">
                <div class="panel-body">
                    @if($child->children->count())
                        @include('user.accordion2', ['user' => $child])
                    @else
                        <p>
                            {{ 'Еще нет команды во 2 цикле' }}
                        </p>
                    @endif
                </div>
            </div>
        </div>

    @endforeach

</div>
