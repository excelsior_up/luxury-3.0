<div class="panel-group" id="accordion{{ $user->id }}" role="tablist" aria-multiselectable="true">

    @foreach($user->children as $child)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{ $child->id }}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $child->id }}" aria-expanded="true" aria-controls="collapse{{ $child->id }}">
                        {{ $child->position }}. {{ $child->name }} - {{ $child->login }}
                    </a>
                </h4>
            </div>
            <div id="collapse{{ $child->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $child->id }}">
                <div class="panel-body">
                    @if($child->children->count())
                        @include('user.accordion', ['user' => $child])
                    @else
                        <p>
                            Нет команды.
                        </p>
                    @endif
                </div>
            </div>
        </div>
    @endforeach

</div>