<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/club" class="site_title"><img src="/images/logo.png" class="logo"><span class="tit-menu">Luxury Life</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu prile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{ $user->avatar }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Здравствуйте,</span>
                <h2>{{ $user->name }} {{ $user->surname }}</h2>
            </div>
        </div>
        <!-- /menu prile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a href="/club"><i class="fa fa-user"></i>Мой профиль</a>
                    </li>
                    <li>
                        <a href="/club/structure"><i class="fa fa-sitemap"></i>Моя структура</a>
                    </li>
                    <li>
                        <a href="/club/my-structure"><i class="fa fa-sitemap"></i>Моя команда</a>
                    </li>
                    {{--<li>
                        <a href="/club/my-structure"><i class="fa fa-sitemap"></i>Моя команда</a>
                    </li>--}}
                    <li>
                        <a href="/club/rules"><i class="fa fa-info-circle"></i>Юридическая служба</a>
                    </li>
                    <li>
                        <a href="/club/marketing"><i class="fa fa-line-chart"></i>Компенсационный план</a>
                    </li>
                    <li>
                        <a href="/club/rules-agreed"><i class="fa fa-key"></i>Регистрация</a>
                    </li>

                    {{-- @if(Auth::club_user()->check())
                        @if(App\HouseUser::where('login', Auth::club_user()->get()->login)->orWhere('email', Auth::club_user()->get()->email)->first() == null)
                            <li>
                                <a href="/club/to-house-program"><i class="fa fa-key"></i>Жилищная программа</a>
                            </li>
                        @endif
                    @endif --}}

                    <li>
                         <a href="/club/news"><i class="fa fa-th-large"></i>Новости</a>
                    </li>
                    <li>
                        <a href="/club/contacts"><i class="fa fa-location-arrow"></i>Контакты</a>
                    </li>
                    <li>
                        <a href="/club/logout"><i class="fa fa-lock"></i>Выход</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">

                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ $user->avatar }}" alt="{{$user->name }} {{ $user->surname }}">
                        {{ $user->name }} {{ $user->surname }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                        <li><a href="/club">Мой профиль</a>
                        </li>
                        <li><a href="/club/logout"><i class="fa fa-sign-out pull-right"></i>Выйти</a>
                        </li>
                    </ul>
                </li>
                  <li>
                   <p class="data">{{ date("d.m.Y") }} <span>{{ date("H:i") }}</span></p>
                </li>
            </ul>
        </nav>
    </div>

</div>
<!-- /top navigation -->
