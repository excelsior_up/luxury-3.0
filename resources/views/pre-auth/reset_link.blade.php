@extends('pre-auth')

@section('content')

<div class="wrapper">
    <div style="margin:auto; margin:100px 0px; " class="card col-md-6 col-md-push-3 col-xs-12">
    <h2>Восстановление пароля</h2>
    {!! Form::open(['url' => $entity == 'admin' ? '/admin/make-password' : '/change-password', 'method' => 'post', 'class' => 'form-horizontal form-label-left' ]) !!}
             
        <div class="row">
                
           
                @if(Session::has("success"))
                    <span style="font-size:20px;">{{ Session::get('success') }}</span>
                @elseif(Session::has('error'))
                    <span style="color:red; font-size:20px;">{{ Session::get('error') }}</span>
                @else
                
                <div class="row">
            
                    <div class="col-xs-12">
                    
                    <div class="item form-group   displayBlock input-field col-xs-12 col-md-8 col-md-push-3">
                              {!! Form::password('password', ['class' => 'validate', 'required','data-validate-length-range' => '6','id'=>'pass']) !!}
                              <label for="password" >Новый пароль</label>
                     </div>

                    <div class="item form-group displayBlock input-field  col-xs-12 col-md-8 col-md-push-3   ">
                               {!! Form::password('password2', ['class' => 'validate', 'required','data-validate-linked' => 'password','id'=>'pass2']) !!}
                              <label for="password2" >Новый пароль</label>
                     </div>

                    </div>
             
                </div>
                
             <button class="waves-effect waves-light btn">Восстановить<div class="ripple-wrapper"></div></button>
        
        </div>
        @endif
    {!! Form::close() !!}
    </div>
</div>
{{ Session::set('error', null)}}
@stop