@extends('landing')

@section('content')

    <div class="wrapper">
        <div style=" margin:auto; margin-top:100px; margin-bottom:100px;" class="materialPost col-md-6 col-md-push-3 col-xs-12">
            <h2>Регистрация</h2>
            {!! Form::open(['class' => 'registrationForm form-horizontal form-label-left']) !!}

                @if (Request::has('token'))
                    {!! Form::hidden('token', Request::input('token')) !!}
                @endif

                @if(Session::has('token-error'))
                    <span style="color:red; font-size:20px;">{{ Session::get('token-error') }}</span>
                @endif

                {{-- Проверка на пригласительую ссылку --}}
                @if(! isset($_GET['token']))
                    {{-- Логин спонсора --}}
                    <div class="item form-group">
                        @if(Session::has('error_sponsor'))
                            <span style="color:red; font-size:20px;">
                                {{ Session::get('error_sponsor') }}
                            </span>
                        @endif

            

                        <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                      {!! Form::text('loginsponsor', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'login']) !!}
                                      <label for="first_name">Логин спонсора</label>
                            </div>  

                        </div>

                    </div>
                @endif

                <div class="item form-group">


                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                     {!! Form::text('login', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'login']) !!}
                                      <label for="login">Логин<span class="required">*</span></label>
                            </div>  

                    </div>

                </div>

                {{-- ------ --}}

                <div class="item form-group">



                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                     {!! Form::text('surname', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'name']) !!}
                                      <label for="surname">Фамилия<span class="required">*</span></label>
                            </div>  

                    </div>

                </div>

                <div class="item form-group">


                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                     {!! Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'name']) !!}
                                      <label for="name">Имя<span class="required">*</span></label>
                            </div>  

                    </div>

                </div>

                <div class="item form-group">

                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                    {!! Form::text('patronymic', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'name']) !!}
                                      <label for="patronymic">Отчество<span class="required">*</span></label>
                            </div>  

                    </div>
                    
                
                </div>

                {{-- ------ --}}

                <div class="item form-group">
                    @if(Session::has('error'))
                        <span style="color:red; font-size:20px;">{{ Session::get('error') }}</span>
                    @endif

                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                    {!! Form::email('email', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'email', 'required']) !!}
                                      <label for="email">E-mail<span class="required">*</span></label>
                            </div>  

                    </div>

                </div>

                <div class="item form-group">

                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                    {!! Form::text('phone', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','data-inputmask'=>"'mask' : '+7(999) 999-99-99'", 'id' => 'phone']) !!}
                                      <label for="phone">Телефон<span class="required">*</span></label>
                            </div>  

                    </div>
                    
              
                </div>

                <div class="item form-group">

                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                {!! Form::text('address', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'address']) !!}

                                      <label for="address">Адрес<span class="required">*</span></label>
                            </div>  

                    </div>
                    
                </div>

                <div class="item form-group">

                   <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                {!! Form::text('bank_account', null, ['class' => 'form-control col-md-7 col-xs-12',  'id' => 'bank_account']) !!}

                                      <label for="bank_account">Банковский счет <span class="required">*</span></label>
                            </div>  

                    </div>
                    
              
                </div>

                <div class="item form-group">

                    <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                {!! Form::text('bik', null, ['class' => 'form-control col-md-7 col-xs-12', 'required', 'id' => 'bik']) !!}

                                      <label for="bik">ИИН <span class="required">*</span></label>
                            </div>  

                    </div>
                </div>

                <div class="item form-group">

                    <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                {!! Form::text('bank_name', null, ['class' => 'form-control col-md-7 col-xs-12',  'id' => 'bank_name']) !!}

                                      <label for="bank_name">Название банка <span class="required">*</span></label>
                            </div>  

                    </div>
                </div>

                <div class="item form-group">
                    
                    <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                                {!! Form::password('password', ['class' => 'form-control col-md-7 col-xs-12', 'required','data-validate-length-range' => '6','id'=>'pass']) !!}

                                      <label for="password">Пароль <span class="required">*</span></label>
                            </div>  

                    </div>
                </div>

                <div class="item form-group">                
                    
                    <div class="row">
                            

                            <div class="input-field col-xs-12 col-md-6 col-md-push-3">
                               {!! Form::password('password2', ['class' => 'form-control col-md-7 col-xs-12', 'required','data-validate-linked' => 'password','id'=>'pass2']) !!}

                                      <label for="password2">Пароль еще раз <span class="required">*</span></label>
                            </div>  

                    </div>

                </div>

                <button class="waves-effect waves-light btn" id="but-save" disabled>Отправить<div class="ripple-wrapper"></div></button>
            {!! Form::close() !!}
        </div>
    </div>

@stop
