@extends('pre-auth')

@section('content')

<div class="wrapper">
	<div class="clearfix"></div>
    <div style=" margin:auto; margin-top:100px; ">
    	<div class="card col-md-6 col-md-push-3 col-xs-12">
    		<h2 class="">Поздравляем! Вы успешно зарегистрировались.</h2>
    		<p class="">Для входа в личный кабинет введите PIN-code или переведите 25 евро по ниже указанным реквизитам, после этого свяжитесь с менеджером компании и назовите точное время перевода платежа, если платеж подтвердится Вам будет выслан PIN-code по СМС или на электронную почту.</p>
            <br>
            
            <div class="">
                <p>
                    <b>Номер карты: </b>4405 6452 2082 1727
                </p>
                <p>
                    <b>Данные владельца: </b>Zhandos Imanbayev
                </p>
                <p>
                    <b>ИИН: </b>760102350484
                </p>
            </div>
    
            <h2 class="aft-reg-title" >
                Пройдите по ссылке для
                
                <a href="signin/pin" class="waves-effect waves-light btn">входа</a>
            </h2>

        </div>

    </div>
</div>

@stop