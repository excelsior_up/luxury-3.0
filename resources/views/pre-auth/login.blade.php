@extends('landing')

@section('content')


    <div class="wrapper">
 
        <div style="padding:1em;margin:100px auto; " class="card col-md-6 col-md-push-3 col-xs-12">
            <h3>Авторизация</h3>

             <div class="row">
                <div class="col s12">
                  <ul class="tabs">
                   
                    <li class="tab col s3">
                        <a href="#club" aria-controls="club" role="tab" data-toggle="tab">Клубная программа</a>
                    </li>
                    <li class="tab col s3">
                        <a href="#house" aria-controls="house" role="tab" data-toggle="tab">Жилищная программа</a>
                     </li>
                   
                  </ul>
                </div>

                <div style="padding:2em 0;" role="tabpanel" class="tab-pane active" id="club">

                    {!! Form::open(['method' => 'post', 'url' => '/club/login', 'class' => 'form-horizontal form-label-left mt15']) !!}

                        @if(Session::has('error-message'))
                            <span style="color:red; font-size:20px;">{{ Session::get('error-message') }}</span>
                            <br><br>
                        @endif
                        <br>
                        @include('pre-auth._login')

                    {!! Form::close() !!}
                </div>

                 <div style="padding:2em 0;" role="tabpanel" class="tab-pane" id="house">

               
                    {!! Form::open(['method' => 'post', 'url' => '/house/login', 'class' => 'form-horizontal form-label-left mt15']) !!}

                        @if(Session::has('error-message'))
                            <span style="color:red; font-size:20px;">{{ Session::get('error-message') }}</span>
                            <br><br>
                        @endif
                        <br>
                        @include('pre-auth._login')

                    {!! Form::close() !!}
                </div>
              </div>


        </div>
    </div>

@stop
