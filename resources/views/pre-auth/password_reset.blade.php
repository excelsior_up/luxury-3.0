@extends('pre-auth')

@section('content')

<div class="wrapper">
    <div style="margin: 100px auto;padding:2em 1em;  " class="card col-md-6 col-md-push-3 col-xs-12">
    <h2>Восстановление пароля</h2>
    {!! Form::open(['url' => $entity == 'admin' ? '/admin/reset' : '/reset', 'method' => 'post', 'class' => 'row form-horizontal form-label-left' ]) !!}
             

        <div class="item form-group row">
            @if(Session::has('error'))
                <span style="color:red; font-size:20px;">{{ Session::get('error') }}</span>
            @endif
            
            <div class="input-field col-md-6 col-md-push-3">
            
                    {!! Form::email('email', null, ['class' => '', 'required','id' => 'email']) !!}
                    <label for="email" class="">E-mail <span class="required">*</span></label>

            </div>
        </div>
        
        <button class="btn btn-primary btn-raised">Восстановить<div class="ripple-wrapper"></div></button>
    {!! Form::close() !!}
    </div>
</div>

@stop