<div class="item form-group input-field col s6">
          <input name="login"  type="text" class="validate" required>
          <label for="first_name">Логин</label>
</div>  

<div class="item form-group input-field col s6">
          <input name="password" required   type="password" class="validate" data-validate-length-range="6">
          <label for="first_name">Пароль</label>
</div>  

@if (Request::is('signin/pin'))
    <div class="displayBlock item form-group input-field col-md-6 col-md-push-3 clearfix">
            {!! Form::text('pin', null, ['class' => 'validate', 'required', 'id' => 'pin']) !!}
            <label for="pin" >PIN код</label>
    </div>

    <button type="submit"  class="col-md-push-4 displayBlock clearfix waves-effect waves-light btn">Войти</button>

@else
    <button type="submit" class="waves-effect waves-light btn">Войти</button>
    <a class="clearfix" href="/reset" style="font-size:20px;">Забыли пароль?</a><br>
@endif



{{-- @if(Session::has('error-message') && Request::is('signin'))
    <p style="color:green; font-size:20px;">
        Если вы уже внесли оплату и имеете PIN-код, то пройдите по следующей
        <a href="/signin/pin">ссылке</a>
    </p>
@endif --}}

