{!! Form::open( ['url' => 'requests', 'files' => true] ) !!}

	<div class="form-control-wrapper">
		{!! Form::label('sender_text', 'Текст заявки:') !!}
		{!! Form::textarea('sender_text', null, [ 'class' => 'form-control' ]) !!}
	</div>
	
	{!! Form::label('sender_file_name', 'Прикрепление:') !!}
	{!! Form::file('sender_file_name', null, [ 'class' => 'btn btn-default' ]) !!}
	<br/>
	
	{!! Form::submit('Отправить заявку', [ 'class' => 'btn btn-primary' ]) !!}
	
{!! Form::close() !!}
	
@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

