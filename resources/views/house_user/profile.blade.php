@extends('registr')

@section('content')

<div class="row x_title">
    <div class="col-md-6">
        <h3>Мой профиль</h3> (Жилищная программа)
    </div>
</div>


<!--  Rates Script -->
<div class="money" id="d1" style="margin:0 auto;background-color:#DFDFDF;width:100%;height:28px;line-height:26px;border:1px solid #DFDFDF;color:#000000;">
    {{--<script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=ru&f=RUB&t=KZTEUR,EURKZT,KZTRUB,RUBKZT,KZTCNY,CNYKZT,KZTUSD,USDKZT,&a=1&d=DFDFDF&n=FFFFFF&o=000000&v=11"></script>--}}
</div>
<!-- End of Exchange Rates Script -->

<div class="col-md-8 col-sm-8 col-xs-12">
    <div class="col-md-3 col-sm-3 col-xs-3">
        <img src="{{ $user->avatar }}" class="ava" id="blah">
        <form action="" method="post" enctype="multipart/form-data" id="form_id">
            <input type="file" class="btn btn-default photo-upl" id="imgInp" name="avatar">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
    <div class="col-md-5 col-sm-5 col-xs-12">
        <p class="text-about"><span>ID: </span> {{ str_pad($user->id, 6, 0, STR_PAD_LEFT) }}</p>
        <p class="text-about"><span>Логин: </span> {{ $user->login }}</p>
        <p class="text-about"><span>E-mail: </span> {{ $user->email }}</p>
        <p class="text-about"><span>ФИО: </span> {{ $user->surname }} {{ $user->name }} {{ $user->patronymic }}</p>
        <p class="text-about"><span>Телефон: </span> {{ $user->phone }} </p>
        <p class="text-about"><span>Адрес: </span> {{ $user->address }} </p>
        <p class="text-about"><span>Мой уровень: </span> Уровень {{ $user->cycle }} </p>
        <a href="/house/edit-profile"class="edit-but btn btn-primary btn-raised">Редактировать профиль</a>
    </div>

    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon">
                </div>
                <div class="count">
                    @for ($k = 0; $k < $user->cycle; $k++)
                        {{ $roman[$k]." " }}
                    @endfor
                </div>
                <p>Мой уровень: {{ $user->cycle }}</p>
            </div>

    </div>
    <div class="col-md-12 col-sm-12">
        <div class="x_panel invite-friend">
            <div class="x_content">
                <div class="row">
                    <h4 align="center"> Пригласить друга </h4>
                    <div align="center">
                        @if(Session::has("success"))
                           <p style="color:green;" align="center"> {{Session::get("success")}}</p>
                        @endif
                    </div>

                    <div class="form-group inv">
                        <button class="btn btn-primary btn-raised generate link" id="generate-link">
                            Сгенерировать ссылку
                            <div class="ripple-wrapper"></div>
                        </button>
                        <input type="text" name="generated_link" id="paste-link" class="form-control link ld" readonly>

                        <button class="btn btn-primary btn-raised generate " style="display:none;" id="email_send" data-toggle="modal" data-target="#myModal">
                            Отправить ссылку на почту
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-sm  " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog  modal-sm" style="width:40%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Отправить ссылку по почте</h4>
      </div>

      {!! Form::open(['url' => '/house/email-send', 'method' => 'post']) !!}
          <div class="modal-body">
                <h5 style="color:red;">Одна ссылка может зарегистрировать только одного человека</h5>
                <label for="" class="control-label">Email</label>
                <input type="email" class="form-control" name="link_email_send" required="">

                <label for="" class="control-label">Ваше сообщение </label>
                <textarea class="form-control" name="link_message_send" ></textarea>

                <label for="" class="control-label">Ваша ссылка</label>
                <input type="text" class="form-control" name="link_send"  id="link_send" readonly>
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <input type="submit" class="btn btn-primary" value="Отправить">
          </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>


@include('includes.right')

<div class="clearfix"></div>

<script>

$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})

    $("#imgInp").change(function(){

        var form = document.getElementById('form_id');
        var formData = new FormData(form);
        $.ajax({
            url: "/house/change-avatar",
            type: "POST",
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            data: formData,

            success: function(result) {
            },
            error:function(result){
                console.log(result);;
            }
        });
    });

    $("#generate-link").click(function() {

        $(this).append('<img src="/images/ajax.gif">');
        that = $(this);

        $.ajax({
            type: 'GET',
            url: '/house/generate-link',
            data: {},
            success: function(result)
            {
                $("#paste-link").val(result);
                $("#email_send").css("display", "block");
                $("#link_send").val(result);
                $("img", that).remove();
            },
            error: function(result)
            {
                console.log(result);
                alert('Перезагрузите страницу и попробуйте еще раз');
            }

        });
    });
</script>

@stop
