@extends('registr')

@section('content')
    <div class="dashboard_graph">

        <div class="row x_title">
            <div class="col-md-6">
                <h3>Регистрация</h3>
            </div>
        </div>

        <div class="col-md-8 col-sm-8 ">
            {!! Form::open(['class' => 'form-horizontal form-label-left']) !!}

            <hr>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="login">
                    Логин <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('login', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'login']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
                    Фамилия <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('surname', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'name']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
                    Имя <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'name']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">
                    Отчество <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('patronymic', null, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'name']) !!}
                </div>
            </div>

            @if(Session::has('email-error'))
                <span style="color:red; font-size:12px;">{{ Session::get('email-error') }}</span>
            @endif
            <div class="item form-group">

                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">
                    E-mail <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::email('email', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'email', 'required']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="phone">Телефон <span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                     {!! Form::text('phone', null, ['class' => 'form-control col-md-7 col-xs-12','data-inputmask'=>"'mask' : '+7(999) 999-99-99'", 'id' => 'phone']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="address">Адрес <span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('address', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'address']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="birth_date">Дата рождения <span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::text('birth_date', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'дд.мм.гггг', 'data-inputmask'=>"'mask': '99.99.9999'", 'id' => 'birth_date']) !!}
                </div>
            </div>
             <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="bank_account">Банковский счет <span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {!! Form::text('bank_account', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'bank_account']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="bik">ИИН<span class="required"></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                {!! Form::text('bik', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'bik']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="bank_name">Название банка<span class="required"></span>
                </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                            {!! Form::text('bank_name', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'bank_name']) !!}
                    </div>
            </div>

            <div class="item form-group">
                <label for="password" class="control-label col-md-2 col-sm-2 col-xs-12">Пароль<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   {!! Form::password('password', ['class' => 'form-control col-md-7 col-xs-12', 'required', 'id' => 'pass', 'data-validate-length-range' => '6']) !!}
                </div>
            </div>

            <div class="item form-group">
                <label for="password2" class="control-label col-md-2 col-sm-2 col-xs-12">Пароль еще раз<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::password('password2', ['class' => 'form-control col-md-7 col-xs-12', 'required', 'id' => 'pass2', 'data-validate-linked' => 'password']) !!}
                </div>
            </div>

            <button class="btn btn-primary btn-raised" id="but-save" disabled>
                Сохранить
                <div class="ripple-wrapper"></div>
            </button>

            {!! Form::close() !!}

        </div>

        @include('includes.right')

        <div class="clearfix"></div>

    </div>

@stop
