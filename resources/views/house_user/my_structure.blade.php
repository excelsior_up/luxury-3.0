@extends('app')

@section('content')


      <h1 align="center">Моя команда</h1>
        <div class="x_content" id="search_results">
          <table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:13px">
              <thead>
                  <tr>
                      <th>Логин</th>
                      <th>Имя</th>
                      <th>Фамилия</th>
                      <th>Отчество</th>
                      <th>Телефон</th>
                      <th>Логин спонсора</th>
                      <th>Дата регистрации</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach ($user->children as $key => $child)
                    @if($key == 1)

                    @endif
                    <tr>
                      <td>
                        <span
                        @if($child->encourager_id == $user->id)
                            class="label label-success"
                            style="font-size: 13px;"
                        @endif
                        >
                            {{$child->login}}
                        </span>
                      </td>
                      <td>{{$child->name}}</td>
                      <td>{{$child->surname}}</td>
                      <td>{{$child->patronymic}}</td>
                      <td>{{$child->phone}}</td>
                      <td>
                          @if($child->encourager_id == $user->id)
                            {{$user->login}}
                          @endif
                          {{--
                          @if($child->old_parent_id == -1)
                            {{$user->login}}
                          @else
                          @endif --}}
                      </td>
                      <td>{{ date('d.m.Y, H:i', strtotime($child->created_at)) }}</td>
                    </tr>
                  @endforeach


              </tbody>

          </table>
            @if(!$user->children->count())
             <h2 align="center">У вас пока нет команды</h2>
            @endif
      </div>
@stop
