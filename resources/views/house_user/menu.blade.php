<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/house" class="site_title"><img src="/images/logo.png" class="logo"><span class="tit-menu">Luxury Life</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu prile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{ $user->avatar }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Здравствуйте,</span>
                <h2>{{ $user->name }} {{ $user->surname }}</h2>
                <br>Жилищная программа
            </div>
        </div>
        <!-- /menu prile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a href="/house"><i class="fa fa-user"></i>Мой профиль</a>
                    </li>
                    <li>
                        <a href="/house/structure"><i class="fa fa-sitemap"></i>Моя структура</a>
                    </li>
                    <li>
                        <a href="/house/my-structure"><i class="fa fa-sitemap"></i>Моя команда</a>
                    </li>
                    <li>
                        <a href="/house/rules"><i class="fa fa-info-circle"></i>Юридическая служба</a>
                    </li>
                    <li>
                        <a href="/house/marketing"><i class="fa fa-line-chart"></i>Компенсационный план</a>
                    </li>
                    <li>
                        <a href="/house/rules-agreed"><i class="fa fa-key"></i>Регистрация</a>
                    </li>

                    <li>
                         <a href="/house/news"><i class="fa fa-th-large"></i>Новости</a>
                    </li>
                    <li>
                        <a href="/house/contacts"><i class="fa fa-location-arrow"></i>Контакты</a>
                    </li>
                    <li>
                        <a href="/house/logout"><i class="fa fa-lock"></i>Выход</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">

                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ $user->avatar }}" alt="{{$user->name }} {{ $user->surname }}">
                        {{ $user->name }} {{ $user->surname }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                        <li><a href="/house">Мой профиль</a>
                        </li>
                        <li><a href="/house/logout"><i class="fa fa-sign-out pull-right"></i>Выйти</a>
                        </li>
                    </ul>
                </li>
                  <li>
                   <p class="data">{{ date("d.m.Y") }} <span>{{ date("H:i") }}</span></p>
                </li>
            </ul>
        </nav>
    </div>

</div>
<!-- /top navigation -->
