

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:85%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        
        <h4 class="modal-title" id="myModalLabel">
          {{ $user->login }} - {{ $user->name . " " . $user->surname }}. 
          Зарегистрировал(-а): {{ $user->encouraged->count() }} человек {{ $user->encouraged->count() ? '(выделены розовым цветом)' : '' }}
        </h4>
        @if ($user->id != 1)
          <p>
          </p>
          <p>

          </p>
        @endif
        
        
      
        
      </div>
      <div class="modal-body" style="overflow-y:scroll; height:500px;">
        <div class="x_content" id="search_results">
          <table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:13px">
              <thead>
                  <tr>
                      <th>Логин</th>
                      <th>Имя</th>
                      <th>Фамилия</th>
                      <th>Телефон</th>
                      <th>Дата регистрации</th>
                  </tr>
              </thead>

              <tbody>
                  @foreach ($user->children as $key => $child) 
                    @if($key == 1)

                    @endif
                    <tr style="{{ $child->encourager_id == $user->id ? 'background-color: pink;' : '' }}">
                      <td>{{$child->login}}</td>
                      <td>{{$child->name}}</td>
                      <td>{{$child->surname}}</td>
                      <td>{{$child->phone}}</td>
                      <td>
                        {{ date('d.m.Y, H:i', strtotime($child->created_at)) }} 
                      </td>
                    </tr>    
                  @endforeach
              </tbody>

          </table>
          @if(!$user->children->count())
             <h2 align="center">Нет команды</h2>
          @endif  
      </div>

          
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

<script>

    $('#myModal').modal()    
</script>
