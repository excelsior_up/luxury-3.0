@extends('registr')

@section('content')

 <div class="row x_title">
    <div class="col-md-6">
        <h3>Новости</h3>
    </div>
</div>
<div class="col-md-8 col-sm-8 ">
    @forelse ($posts as $post)
        <div class="col-md-4">
            <a href="/user/news-page/{{ $post->id }}">
                <div class="thumbnail">
                    <div class="image view view-first">
                        <img style="width: 100%; display: block;" src="{{$post->image}}" alt="image" />
                        <div class="mask">
                            <p>{!! $post->title !!}</p>
                        </div>
                    </div>
                    <div class="caption">
                        <p>{!! $post->description !!}</p>
                    </div>
                </div>
            </a>
        </div>
    @empty
        <p>На данные момент на сайте нет новостей.</p>
    @endforelse
</div>

@include('includes.right')

<div class="clearfix"></div>

@stop