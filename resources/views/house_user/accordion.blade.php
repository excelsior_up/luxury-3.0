<div class="panel-group" id="accordion{{ $user->id }}" role="tablist" aria-multiselectable="true">

    @foreach($user->children as $child)
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{ $child->id }}">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $child->id }}" aria-expanded="true" aria-controls="collapse{{ $child->id }}">
                        {{ $child->login }}
                    </a>
                </h4>
            </div>
            <div id="collapse{{ $child->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $child->id }}">
                <div class="panel-body">
                    @if($child->children->count())
                        @include('house_user.accordion', ['user' => $child])
                    @else
                        <p>
                            {{ '' }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    @endforeach

</div>
