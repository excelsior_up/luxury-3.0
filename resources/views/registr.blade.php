<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.ico">

    <title>Luxury Life</title>

    <!-- Bootstrap core CSS -->

    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">

    <link href="/css/roboto.min.css" rel="stylesheet">
    <link href="/css/material-fullpalette.min.css" rel="stylesheet">
    <link href="/css/ripples.min.css" rel="stylesheet">
    <link href="/css/snackbar.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/icheck/flat/green.css" rel="stylesheet" />
    <link href="/css/floatexamples.css" rel="stylesheet" type="text/css" />
    <link href="/css/vendor/calendar/calendar.css">
    <link href="/css/datepicker/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.min.js"></script>

    <script src="/js/vendor/jquery.browser.min.js"></script>

    <script>
        NProgress.start();
    </script>


    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>x
    <![endif]-->


</head>

<body class="nav-md">

@include('includes.analytics')

<div class="container body">
    <div class="main_container">

        @if (Auth::house_user()->check())
            @include('house_user.menu')
        @else
            @include('user.menu')
        @endif
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashboard_graph">
                        @yield('content')
                    </div>
                </div>
            </div>
            <!-- footer content -->
            <footer>
                <div class="">
                    <p align="center">Все права защищены 2015
                    </p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
        <!-- /page content -->

    </div>
</div>
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <script src="/js/custom.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <script src="/js/inputmask/jquery.inputmask.js"></script>
    <script src="/js/skycons/skycons.js"></script>
    <script src="/js/validator/validator.js"></script>


<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')

            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);
</script>




<script src="/js/ripples.min.js"></script>
<script src="/js/material.min.js"></script>
<script src="/js/snackbar.min.js"></script>
<script src="/js/datepicker/bootstrap-datepicker.js"></script>
    <script>
    $('.datepick').datepicker({
    endDate: "today",
    autoclose: true,
    startView: 1,
    todayHighlight: true,
    language: "ru",
     orientation: "bottom right",
    defaultViewDate: { year: 2015, month: 06, day: 01 }
});
</script>
<script>
</script>

<script>
    var icons = new Skycons({
                "color": "#73879C"
            }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

    for (i = list.length; i--;)
        icons.set(list[i], list[i]);

    icons.play();
</script>
<!-- /footer content -->

<script src="/js/jquery.nouislider.min.js"></script>

<script>
    $(document).ready(function() {

        // This command is used to initialize some elements and make them work properly
        $.material.init();
        $('#calendar').datepicker({
            inline: true,
            firstDay: 1,
            showOtherMonths: true,
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
        });

    });
</script>

</body>

</html>
