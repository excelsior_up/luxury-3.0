<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="date=no">
<meta name="format-detection" content="address=no">
</head>
<body>
 		
 		<h2 align="center">Вам пришло сообщение с Клуба правовой защиты.</h2>
 		<div align="center" style="font-size:18px">
 			<p>
 				Уважаемый(ая) {{ $user->surname . " " .$user->name }}
 			</p>
	 		<p>
	 		Для восстановления пароля вам нужно будет пройти по этой ссылке {{ $link }}
			</p>
			
		</div>
</body>
</html>