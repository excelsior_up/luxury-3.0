<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="date=no">
<meta name="format-detection" content="address=no">
</head>
<body>
 		
 		<h2 align="center">Вам пришло сообщение с Клуба правовой защиты.</h2>
 		<div align="center" style="font-size:18px">
	 		<p>
	 		Прислал(a) вам это сообщение {{ $user->surname . " " . $user->name . " ". $user->patronymic }}

			</p>
			<p>
			{{ $full_message }}. 
			</p>
			<p>
			Если вы приняли решение зарегистрироваться пройдите по этой ссылке {{ $link }}.
			</p>
		</div>
</body>
</html>