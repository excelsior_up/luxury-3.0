<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="date=no">
<meta name="format-detection" content="address=no">
</head>
<body>
 		
 		<h2 align="center">Вам пришло сообщение с Клуба правовой защиты.</h2>
 		<div align="center" style="font-size:18px">
 			<p>
 				Уважаемый(ая) администратор сайта LuxuryLife.us
 			</p>
	 		<p>
	 		На сайте появилась новая заявка от пользователя.
			</p>
			<p>Ознакомиться с ней вы можете в административной панели на {!! link_to('admin/requests', 'сайте') !!}.</p>
		</div>
</body>
</html>
