<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Ubuntu&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<meta charset="utf-8">
		<link rel="shortcut icon" href="/images/favicon.ico">
		<title>Сайт недоступен</title>
		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Ubuntu';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
				font-weight: bold;
				color: black;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">
					<p>
						Сайт временно недоступен. 
					</p>
					<img src="https://eecosphere.com/assets/ajax-loader-281fdba9b1249c8c21766edd43c705e7.gif" width="150">
				</div>
				<p>
					Пожалуйста, свяжитесь с владельцем сайта, чтобы как можно скорее решить эту проблему.
				</p>
			</div>
		</div>
	</body>
</html>
