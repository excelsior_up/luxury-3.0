@extends('admin')

@section('content')

<div class="row x_title">
    <div class="col-md-6">
        <h3>Мой профиль</h3>
    </div>
</div>

<!--  Rates Script -->
<div class="money" id="d1" style="margin:0 auto;background-color:#DFDFDF;width:100%;height:28px;line-height:26px;border:1px solid #DFDFDF;color:#000000;">
    <script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=ru&f=RUB&t=KZTEUR,EURKZT,KZTRUB,RUBKZT,KZTCNY,CNYKZT,KZTUSD,USDKZT,&a=1&d=DFDFDF&n=FFFFFF&o=000000&v=11"></script>
</div>
<!-- End of Exchange Rates Script -->

<div class="col-md-8 col-sm-8 ">
    <div class="col-md-3 col-sm-3 col-xs-3">   
        <img src="{{$user->avatar}}" class="ava" id="blah">
        <form action="" method="post" enctype="multipart/form-data" id="form_id">
            <input type="file" class="btn btn-default photo-upl" id="imgInp" accept="image/*" name="avatar">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>

    <div class="col-md-5 col-sm-5">
        <p class="text-about"><span>Логин: </span> {{ $user->login }}</p>
        <p class="text-about"><span>ФИО: </span> {{ $user->name }} {{ $user->surname }}</p>
        <p class="text-about"><span>E-mail: </span> {{ $user->email }}</p>
        <a href="/admin/edit-profile"class="edit-but btn btn-primary btn-raised">Редактировать профиль</a>
    </div>
    
   
</div>
<script>
    $(document).ready(function() {

        // This command is used to initialize some elements and make them work properly
        $.material.init();
        $('#calendar').datepicker({
            inline: true,
            firstDay: 1,
            showOtherMonths: true,
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
        });

    });
</script>


@include('includes.right')

<div class="clearfix"></div>
<script>
    $("#imgInp").change(function(){

        var form = document.getElementById('form_id');
        var formData = new FormData(form);
        $.ajax({
            url: "/admin/change-avatar",
            type: "POST",
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            data: formData,
            
            success: function(result) { 
            },
            error:function(result){
                console.log(result);;
            }
        });
    });
</script>
@stop