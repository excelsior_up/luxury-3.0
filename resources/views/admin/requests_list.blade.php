@extends('admin')

<style>
	.td-hover
	{
		cursor: pointer;
		text-decoration: underline;
	}
	.is-read:hover
	{
		text-decoration: none;
	}

</style>

@section('content')
	<div class="row x_title">
		<div class="col-md-6">
			<h3>Список заявок</h3>
		</div>
	</div>
	@if (count($requests))
	<div class="x_content" id="search_results">
		<table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:13px">
	
			<thead>
				<tr class="headings">
					<th>  </th>
					<th> ID </th>
					<th> Пользователь </th>
					<th class="mail-col"> Email </th>
					<th> Текст </th>
					<th> Прикрепление </th>
					<th> Дата </th>
				</tr>
			</thead>
		
			@foreach ($requests as $current_request)
			<tbody>
				<tr>
					<td> 
						@if ($current_request->is_read)
							<a href="/user_request/checked/{{ $current_request->id }}" class="is-read">
								&nbsp;&nbsp;&nbsp;
							</a>
						@else
							<a href="/user_request/checked/{{ $current_request->id }}" class="is-read">
								<i class="glyphicon glyphicon-certificate"></i>
								&nbsp;
							</a>
						@endif
					</td>
					<td> {{ $current_request->id }} </td>
					<td> 
						{{ $current_request->name }} 	
					</td>
					<td> {{ $current_request->email }} </td>
					<td> {{ $current_request->text }} </td>
					<td> 
						@if ( $current_request->has_file )	
							<a href="/user_request/download/{{ $current_request->id }}">
								<i class="glyphicon glyphicon-download"></i>
								{{ $current_request->file_name }}
							</a>
						@else
							---
						@endif
					</td>
					<td> {{ $current_request->created_at->format("d-m-Y, H:i") }} </td>
				</tr>
			</tbody>
			@endforeach
		</table>
	</div>
	@else
		Нет заявок
	@endif
	
	<div id="pagination">
		{!! $requests->render() !!}
	</div>
	
@stop
