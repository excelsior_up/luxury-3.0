@extends('admin')

@section('content')
	<div class="row x_title">
        <div class="col-md-6">
            <h3>Структура</h3>
        </div>
    </div> 

    <div class="x_panel">
        <div class="x_content">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">1-й цикл</a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">2-й цикл</a>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                        @if($user->parent)
                            <h1>
                                Cпонсор: {{ $user->parent->login }}
                            </h1>
                        @endif

                        @if($user->children->count())
                            @include('user.accordion', ['user' => $user])
                        @else
                            <p>
                                {{ 'Нет команды.' }}
                            </p>
                        @endif

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        <p>Вы еще не перешли на 2 цикл.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="subscriber">

    </div>

@stop
