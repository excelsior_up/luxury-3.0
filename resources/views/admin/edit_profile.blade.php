@extends('admin')

@section('content')
	<div class="row x_title">
        <div class="col-md-6">
            <h3>Редактировать профиль</h3>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 ">
    @if (Session::has("error_edit"))
        <p style="color:red;" align="center">{{Session::get("error_edit")}}</p>
    @endif
    {!! Form::open(['url' => '/admin/edit-profile', 'method' => 'post','class' => 'form-horizontal form-label-left']) !!}
        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-md-offset-1 col-xs-12" for="surname">
                Фамилия <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                {!! Form::text('surname', $user->surname, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'surname']) !!}
            </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-md-offset-1 col-xs-12" for="name">
                Имя <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                {!! Form::text('name', $user->name, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'name']) !!}
            </div>
        </div>        

        <div class="item form-group">
            <label class="control-label col-md-2 col-sm-2 col-md-offset-1 col-xs-12" for="email">
                Email <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                {!! Form::text('email', $user->email, ['class' => 'form-control col-md-7 col-xs-12', 'required','id' => 'email']) !!}
            </div>
        </div>
            

        <button class="btn btn-primary btn-raised">Сохранить<div class="ripple-wrapper"></div></button>
    {!! Form::close() !!}
    <button class="btn btn-primary btn-raised" data-toggle="modal" data-target="#complete-dialog">Изменить пароль<div class="ripple-wrapper"></div></button>
    </div>
    <div id="complete-dialog" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" >&times;</button>
                      
                    </div>
                    {!! Form::open(['url' => '/admin/change-password', 'method' => 'post','class' => 'form-horizontal form-label-left']) !!}
                        <div class="modal-body">
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Старый пароль <span class="required"></span>
                                 </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" name="old_password"  class="form-control col-md-7 col-xs-12"   type="password" required>
                                 </div>
                            </div>
            
                            <div class="item form-group">
                                    <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Новый пароль</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="pass" type="password" name="new_password" data-validate-length-range="6" class="form-control col-md-7 col-xs-12" required="required">
                                        </div>
                            </div>
                            <div class="item form-group">
                                    <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Новый пароль еще раз</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                             <input id="pass2" type="password" name="new_password2" data-validate-linked="new_password" class="form-control col-md-7 col-xs-12" required="required">
                                        </div>
                            </div>
                             
                         
                        </div>
                        <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">Отменить</button>
                              <button type="submit" class="btn btn-primary" id="but-save" >Сохранить</button>
                        </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
    
    @include('includes.right')
    
    <div class="clearfix"></div>


@stop