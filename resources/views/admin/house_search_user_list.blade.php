<table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:13px">
	<thead>
	    <tr class="headings">
	        <th>ID </th>
	        <th>Логин</th>
	        <th>Имя</th>
	        <th>Фамилия</th>
	        <th>Отчество</th>
	        <th class="mail-col">E-mail</th>
	        <th>Регистрация</th>
	        <th>Банк.счет</th>
	        <th>Цикл</th>
	        <th class="no-link last">PIN</th>
	    </tr>
	</thead>

	<tbody id="load">
	    @foreach ($users_list as $key => $user)
	    	@if ($key%2 == 0)
				<?php $class = "even"; ?>
			@else
				<?php $class = "odd"; ?>
	    	@endif
	    	<tr class="{{ $class }} pointer">
	            <td>{{ str_pad($user->id, 6, 0, STR_PAD_LEFT) }}</td>
	            <td class="td-hover" onclick="getUser({{$user->id}})">{{ $user->login }}</td>
	            <td>{{ $user->name }}</td>
	            <td>{{ $user->surname }}</td>
	            <td>{{ $user->patronymic }}</td>
	            <td>{{ $user->email }}</td>
	            <td>{{ date('d.m.Y, H:i', strtotime($user->created_at)) }}</td>
	            <td class="a-right a-right ">{{ $user->bank_account }}</td>
	            <td>
	            	@if (!$user->PIN)
	        			<span id="status-{{$user->id}}" style="line-height:20px">Ждем оплаты</span>
	            	@else
	            		{{ $user->cycle }}
	            	@endif
	        	</td>
	            <td class="last">
	        		@if (!$user->PIN)
	        			<a href="#" class="make-pin" onclick="makePIN(this)" data-user-id="{{ $user->id }}">Выдать</a>
	    			@else
	    				{{ $user->PIN }}
	        		@endif
	        	</td>
	        </tr>
	    @endforeach
	</tbody>

</table>