@extends('app')

@section('content')
	<div class="row x_title">
        <div class="col-md-6">
            <h3>Структура</h3>
        </div>
    </div>

    <div class="row ">


                        @if($user->parent)
                            <h2>
                                Cпонсор: {{ $user->parent->login }}
                            </h2>
                        @endif
                        <div class="row" style="text-align: center">
                            <span style="font-size: 150%;">
                                {{$user->login}}
                            </span>
                        </div>
                        @if($user->children->count())
                            <div class="row">
                                @foreach($user->children as $child1)
                                    <div class="col-md-6" style="text-align: center;">
                                        <span style="font-size: 125%;">
                                            {{ $child1->login }}
                                        </span>
                                        @if($child1->children->count())
                                            <div class="row">
                                                @foreach($child1->children as $child2)
                                                    <div class="col-md-6" style="padding-top: 5px;">
                                                        {{ $child2->login }}
                                                        @if($child2->children->count())
                                                            @include('house_user.accordion', ['user' => $child2])
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>

                        @else
                            <p>
                                {{ 'Нет команды.' }}
                            </p>
                        @endif


    </div>

    <div id="subscriber">

    </div>

@stop
