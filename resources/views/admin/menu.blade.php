<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="/admin/profile" class="site_title"><img src="/images/logo.png" class="logo"><span class="tit-menu">Luxury Life</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu prile quick info -->
        <div class="clearfix profile">
            <div class="profile_pic" style="background-image: url('{{ $user->avatar }}')">
                <!-- <img src="{{ $user->avatar ? $user->avatar : '/images/user.png' }}" alt="..." class="img-circle profile_img"> -->
            </div>
            <div class="profile_info">
                <span>Здравствуйте,</span>
                <h2>{{ $user->name }} {{ $user->surname }}</h2>
            </div>
        </div>
        <!-- /menu prile quick info -->

        <br />

         <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <a href="/admin/profile">  
                        <li><i class="fa fa-user"></i>Мой профиль
                    </a>
                    </li>
                    <li>
                        <a href="/admin/rules"><i class="fa fa-info-circle"></i>
                            Юридическая служба
                        </a>
                    </li>
                     <li>
                        <a href="/admin/marketing"><i class="fa fa-line-chart"></i>
                            Компенсационный план
                        </a>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-th-large"></i>
                            Новости <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu" style="display: none;">
                            <li>
                                <a href="/admin/news">Просмотр</a>
                            </li>
                            <li>
                                <a href="/admin/add-news">Добавление</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-list"></i>
                            Список пользователей <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu" style="display: none;">
                            <li>
                                <a href="/admin/users-list">
                                    Клубная программа
                                    <?php
                                    $new_club_users = App\ClubUser::where('PIN', NULL)->count();
                                    ?>
                                    @if ($new_club_users)
                                        <span class="badge" id="num-of-users">({{ $new_club_users }})</span>
                                    @endif
                                </a>
                            </li>
                            <li>
                                <a href="/admin/house-list">
                                    Жилищная программа
                                    <?php
                                    $new_house_users = App\HouseUser::where('PIN', NULL)->count();
                                    ?>
                                    @if ($new_house_users)
                                        <span class="badge" id="num-of-users">({{ $new_house_users }})</span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/admin/contacts"><i class="fa fa-location-arrow"></i>Контакты</a>
                    </li>
                    <li>
                        <a href="/admin/requests">
                        	<i class="glyphicon glyphicon-align-left"></i>
                        	&nbsp;&nbsp;Заявки
                        	<?php 
                                $new_user_requests = App\UserRequest::where('is_read', 0)->count();
                             ?>
                            @if ($new_user_requests)
                                <span class="badge" id="num-of-users">({{ $new_user_requests }})</span>
                            @endif
                        
                        </a>
                    </li>
                    <li>
                        <a href="/admin/logout"><i class="fa fa-lock"></i>Выход</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
               
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ $user->avatar ? $user->avatar : '/images/user.png' }}" alt="">
                            {{ $user->name }} {{ $user->surname }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                        <li><a href="/admin/profile">Мой профиль</a>
                        </li>
                        <li><a href="/admin/logout"><i class="fa fa-sign-out pull-right"></i>Выйти</a>
                        </li>
                    </ul>
                </li>
                  <li>
                   <p class="data">{{ date("d.m.Y") }} <span>{{ date("H:i") }}</span></p>
                </li>
            </ul>
        </nav>
    </div>

</div>
<!-- /top navigation -->
