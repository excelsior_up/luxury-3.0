@extends('admin')

<style>
	.td-hover
	{
		cursor: pointer;
		text-decoration: underline;
	}

</style>

@section('content')
	<div class="row x_title">
	    <div class="col-md-6">
	        <h3>Список пользователей (Жилищная программа)</h3>
	    </div>
	</div>
	<div class="col-md-12 col-sm-12">
	    <div class="x_panel">
	    <div class="x_title">
	        <div class="clearfix"></div>
	    </div>
	    <div align="right">
	    	Поиск: <input type="text"  id="search_id" oninput="SearchResults()">
	    </div>
	    <div class="x_content" id="search_results">
	        <table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:13px">
	            <thead>
	                <tr class="headings">
	                    <th>ID </th>
	                    <th>Логин</th>
	                    <th>Имя</th>
	                    <th>Фамилия</th>
	                    <th>Отчество</th>
	                    <th class="mail-col">E-mail</th>
	                    <th>Регистрация</th>
	                    <th>Банк.счет</th>
	                    <th>Цикл</th>
	                    <th class="no-link last">PIN</th>
	                </tr>
	            </thead>

	            <tbody id="load">
	                @foreach ($users_list as $key => $useritem)
	                	@if ($key%2 == 0)
							<?php $class = "even"; ?>
						@else
							<?php $class = "odd"; ?>
	                	@endif
	                	<tr class="{{ $class }} pointer">
		                    <td>{{ str_pad($useritem->id, 6, 0, STR_PAD_LEFT) }}</td>
		                    <td class="td-hover" onclick="getUser({{$useritem->id}})">{{ $useritem->login }}</td>
		                    <td>{{ $useritem->name }}</td>
		                    <td>{{ $useritem->surname }}</td>
		                    <td>{{ $useritem->patronymic }}</td>
		                    <td>{{ $useritem->email }}</td>
		                    <td>{{ date('d.m.Y, H:i', strtotime($useritem->created_at)) }}</td>
		                    <td class="a-right a-right ">{{ $useritem->bank_account }}</td>
		                    <td>
		                    	@if (!$useritem->PIN)
	                    			<span id="status-{{$useritem->id}}" style="line-height:20px">Ждем оплаты</span>
		                    	@else
		                    		{{ $useritem->cycle }}
		                    	@endif
	                    	</td>
		                    <td class="last">
	                    		@if (!$useritem->PIN)
	                    			<a href="#" onclick="makePIN(this)" data-user-id="{{ $useritem->id }}">Выдать</a>
                    			@else
                    				{{ $useritem->PIN }}
	                    		@endif
	                    	</td>
		                </tr>
	                @endforeach
	            </tbody>

	        </table>
	        
	    </div>
	    <div align="center" id="loading">
	        	
	    </div>

	    <div id="pagination">
	    	{!! $users_list->render() !!}
	    </div>
	    
	</div>

</div>

<div class="clearfix"></div>

<div id="subscriber">
	
</div>



<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:10%; margin-top:15%;">
    
       <img src='/images/loading.gif' height='100px'>
  	<button type="button" style="visibility:hidden;" class="close" data-dismiss="modal" >
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
</div>

<script>
	function getStructure(id)
	{
		$('#myModal1').modal() ; 
		$.ajax({
            url: "/admin/one-structure",
            type: "GET",
            dataType: "text",
            data: {id:id},
            success: function(result) {  
            	$('.close').click() ; 
                $("#subscriber").html(result); 
            },
            error:function(result){
                console.log(result);
            }
        }); 
	}

    function getUser(id)
    {
        window.location = '/admin/see-house-user/' + id;
    }
	
	function makePIN(e)
	{	
		id = $(e).data('user-id');
		that = $(e).html('<img src="/images/ajax.gif">');
		$.ajax({
			type: 'GET',
			url: 'house-generate-pin',
			data: {id: id},
			success: function(pin)
			{
				that.replaceWith(pin);
				$("#status-"+id).replaceWith(1);
				num = parseInt($("#num-of-users").text());
				$("#num-of-users").html(num - 1);
			},
			error: function()
			{
				alert('Произошла ошибка. Пожалуйста, перезагрузите страницу и попробуйте еще раз.');
			}
		});
	}

	function SearchResults()
	{
		var search = $("#search_id").val();
		$("#load").html("");  
		$("#loading").html("<img src='/images/loading.gif' height='100px'>");
		$("#pagination").html("");
		$.ajax({
            url: "/admin/house-search",
            type: "GET",
            dataType: "text",
            data: {search:search},
            success: function(result) {  
            	$("#loading").html("");
                $("#search_results").html(result); 
            },
            error:function(result){
                console.log(result);
            }
        }); 
	}

</script>

@stop
