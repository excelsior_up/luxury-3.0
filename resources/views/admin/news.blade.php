@extends('admin')

@section('content')
    <div class="row x_title">
        <div class="col-md-6">
            <h3>Новости</h3>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 ">
    @foreach($posts as $post)
        <div class="col-md-4">
            <a href="show/{{$post->id}}">
            <div class="thumbnail">
                <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="{{$post->image}}" alt="image" />
                    <div class="mask">
                        <p>{{$post->title}} </p>
                    </div>
                </div>
                
                <div class="caption">
                    <p align="right"><a href="/admin/edit-news/{{$post->id}}" ><i class="fa fa-pencil"></i></a> <a href="/admin/remove-news/{{$post->id}}" ><i class="fa fa-times"></i></a></p>
                    <p>{{$post->description}} </p>
                </div>
                
            </div>
        </a>

        </div>
    @endforeach      
                
    </div>
    
    @include('includes.right')
    
    <div class="clearfix"></div>

@stop