@extends('admin')

@section('content')

    <div class="row x_title">
        <div class="col-md-6">
            <h3>Добавление новости</h3>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 ">
    {!! Form::open(['url' => '/admin/edit-news', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <input type="hidden" name="post_id" value="{{$posts->id}}">
            <div class="form-control-wrapper">
                <!-- <div class="floating-label"></div> -->
                <label>Название новости</label>
                <input type="text" name="title" value="{{$posts->title}}" class="form-control empty" required>
                <span class="material-input"></span>
            </div>
            <label for="message">Краткое описание (100 max) :</label>
            <textarea id="message" required="required" class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" maxlength="100">{{$posts->description}}</textarea>
            <br>
            <label for="fd">Добавить картинку для главной</label>
            <input type="file" name="image_name"><br>
            <h2>Полное описание</h2>
            <textarea id="edit" name="news">{{$posts->post}}</textarea>
            <button class="btn btn-primary btn-raised">Сохранить<div class="ripple-wrapper"></div></button>
        
         <img src="/images/logo.png" class="logo">
         {!! Form::close() !!}
    </div>
    
    @include('includes.right')
    
    <div class="clearfix"></div>
    
@stop