@extends('landing')

@section('content')

    <div class="wrapper">
        <div style="margin:100px auto; " class="card col-md-6 col-md-push-3 col-xs-12">
            <h2 style="color:black !important;">Авторизация</h2>
            
            <div class="row">
                
                {!! Form::open(['class' => 'form-horizontal form-label-left']) !!}

                    @if(Session::has('error-message'))
                        <span style="color:red; font-size:20px;">{{ Session::get('error-message') }}</span>
                        <br>
                        <br>
                    @endif

                    @include('pre-auth._login')

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@stop
