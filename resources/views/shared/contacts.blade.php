@extends($extends)

@section('content')
	<div class="row x_title">
        <div class="col-md-6">
            <h3>Контакты</h3>
        </div>
    </div>
    <div class="col-md-8 col-sm-8">
    
        <div class="text-center">
            <h3>Luxury Life</h3>
        </div>    

        <hr>

        <div class="row invoice-info" style="font-size:18px">
            <div class="col-md-6 text-right">
                <p>
                    <i class="glyphicon glyphicon-globe"></i>
                    Республика Казахстан
                </p>
                <p>
                    <i class="fa fa-building-o"></i>
                    г. Караганда
                </p>
                <p>
                    <i class="glyphicon glyphicon-road"></i>
                    ул. Н.Абдирова, 43
                </p>
            </div>

            <div class="col-md-6">
                <p>
                    <i class="glyphicon glyphicon-phone"></i>
                    +7 (778) 109-02-94
                </p>
                <p>
                    <i class="glyphicon glyphicon-phone"></i>
                    +7 (777) 427-64-15
                </p>
                <p>
                    <i class="glyphicon glyphicon-envelope"></i>
                    <a href="mailto:info@luxurylife.us">
                        info@luxurylife.us
                    </a>
                </p>
            </div>           
        </div>
    
        <hr>

        <div class="row text-center" style="font-size:18px">
            <h4>Телефоны наших юристов: </h4>

            <p>
                <i class="glyphicon glyphicon-phone"></i>
                +7 (747) 875-31-67
            </p>
            <p>
                <i class="glyphicon glyphicon-phone"></i>
                +7 (775) 351-37-64
            </p>
        </div> 

        <hr>

        <div class="row text-center">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                Оставить заявку
            </button>
        </div>
        <div class="row text-center">
            @if (count($errors) > 0)
				<div class="alert alert-danger">
					<h4>
						Что-то пошло не так: заявка не отправлена!
					</h4>
					<h5>
						Попробуйте исправить:
					</h5>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
        </div>

        <hr>

        <div class="text-center">
            <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=TS-aXTdmysxdklxteGQRc2DYjxtfjiW1&width=100%&height=450"></script>
        </div>

        <hr>
        
    </div>
    
    @include('includes.right')

    <div class="clearfix"></div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Оставить заявку
                    </h4>
                </div>
                <div class="modal-body">
                    @include ('user-requests.send_request')
                </div>
            </div>
        </div>
    </div>

@stop
