<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  
  <title>Luxury Life | Клуб правовой защиты</title>

  <meta name="description" content="Клуб правовой защиты «Luxury life» — это юридическая мобильная служба, которая предлагает консультационные услуги юриста." />
  <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">

  <link rel="shortcut icon" href="/images/favicon.ico">

  <meta name="author" content="">

  <link href="../css/custom.css" rel="stylesheet">

  <link href='/landing_resources/css/bootstrap.min.css' rel='stylesheet' type='text/css'>

 

  <link rel="stylesheet" type="text/css" href="/landing_resources/css/custom.css" />

  <link rel="stylesheet" type="text/css" href="/landing_resources/css/style.css" />

   <link href="../css/datepicker/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />

  <link href='/landing_resources/css/main.css' rel='stylesheet' type='text/css'>


  <noscript>
    <link rel="stylesheet" type="text/css" href="css/styleNoJS.css" />
  </noscript>
   
  <script src="../js/jquery.min.js"></script>
  <style>


    .navbar-header {
    background: none;
  }
  h2{
    font-size: 36px !important;
    margin-bottom: 50px;
  }
  .control-label{
    font-size: 18px;
    text-align:left;
    color: white !important;
    margin-bottom: 20px !important;
  }
  .form-control{
    font-size: 22px;
    color: white !important;
    font-weight: bold;
  }
  .datepicker-dropdown{
    z-index: 999999 !important;
  }
  #snackbar-container{
    z-index: 9999 !important;
  }
  body
  {
    background: url(/landing_resources/img/pravo.jpg);
    background-repeat: no-repeat;
    background-size: auto;
  }
  </style>

  <link href='/css/materialize.min.css' rel='stylesheet' type='text/css'>
</head>
<body>

    <div id="loader-wrapper">
      <div id="loader"></div>

      <h1 id="LL"  style="color: white;position: absolute;top: 60%;z-index: 11111;text-shadow: 0px 0px 34px rgba(255, 255, 255, 0.61);"> Luxury Life </h1>

      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>

    </div>
  @include('includes.analytics')

  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="">
     
      <div class="col-md-9 col-xs-12  left text-left">
        
              <div class="navbar-header col-xs-12">
                  <button type="button" class="navbar-default navbar-toggle" data-toggle="collapse" data-target="#navmenu"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand page-scroll logo" href="/" style="width:100%">
                  <img src="/landing_resources/img/logo.png" class="logo"  style="display:inline-block">
                  <img src="/landing_resources/img/mogomogo.png" class="mogomogo img-responsive" style="display:inline-block">
              </a>
              </div>
      </div>

      <div class="col-md-3 col-xs-12 collapseIt">
        <div class="navbar-collapse left collapse" id="navmenu">

          <ul class="nav navbar-nav">
           
             <li><a href="/rules-agreed">Регистрация</a></li>        
             <li id="loginLink"><a  class="loginLink" href="/signin">Войти</a></li>
            
          </ul>

        </div>
      </div>

     </div>

</nav>



  @yield('content')

  @include('includes.footer')

    <script>
        $(document).ready(function () {
            $(":input").inputmask();
              
            setTimeout(function(){
              $('body').addClass('loaded');
            }, 3000);
        });
    </script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/inputmask/jquery.inputmask.js"></script>
    
    <script src="../js/materialize.min.js"></script>
    <script src="../js/validator/validator.js"></script>


    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);
    </script>


    <script src="../js/datepicker/bootstrap-datepicker.js"></script>
    <script>
    $('.datepicker').datepicker({
    endDate: "today",
    autoclose: true,
    startView: 1,
    todayHighlight: true,
    language: "ru",
    defaultViewDate: { year: 2015, month: 06, day: 01 }
});
</script>
    <script src="../js/jquery.nouislider.min.js"></script>

</body>
</html>
