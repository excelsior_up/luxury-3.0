<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSurnamePatrHouseUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('house_users', function(Blueprint $table)
		{
            $table->string('surname');
            $table->string('patronymic');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('house_users', function(Blueprint $table)
		{
            $table->dropColumn('surname');
            $table->dropColumn('patronymic');
		});
	}

}
