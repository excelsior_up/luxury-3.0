<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubUsers2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('club_users_2', function(Blueprint $table)
		{
            $table->increments('id');

            $table->integer('encourager_id');
            $table->integer('parent_id');
			$table->integer('cycle');
			$table->integer('level');
            $table->integer('position');

            $table->tinyInteger('active');
			$table->string('PIN', 4)->nullable();
			$table->string('login')->unique();

            $table->string('surname');
            $table->string('name');
            $table->string('patronymic');

			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('phone');
			$table->integer('card_id');
			$table->string('bank_account');
			$table->string('bik');
			$table->string('bank_name');
			$table->string('address');
			$table->date('birth_date');
			$table->string('avatar');
			$table->rememberToken();
			$table->timestamp('activated_at');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_users_2');
	}

}
