<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEncouragerIdToHouseUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('house_users', function(Blueprint $table)
		{
			$table->integer('encourager_id')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('house_users', function(Blueprint $table)
		{
			$table->dropColumn('encourager_id');
		});
	}

}
