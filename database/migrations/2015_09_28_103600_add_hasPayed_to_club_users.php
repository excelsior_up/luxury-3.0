<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasPayedToClubUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('club_users', function(Blueprint $table)
		{
			$table->string('hasPayed')->after('active')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('club_users', function(Blueprint $table)
		{
			$table->dropColumn('hasPayed');
		});
	}

}
