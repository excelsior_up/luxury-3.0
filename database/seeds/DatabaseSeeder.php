<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\ClubUser;
use App\HouseUser;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		//$this->call('ClubUsersTableSeeder');
        $this->call('HouseUsersTableSeeder');
	}

}

class ClubUsersTableSeeder extends Seeder {

    public function run()
    {
        Model::unguard();
        $arr = ['E','F','G'];
        foreach ($arr as $e1){
            foreach ($arr as $e2){
                foreach ($arr as $e3){
                    foreach ($arr as $e4){
                		$user = new ClubUser();

                		$user->login = $e1 . $e2 . $e3 . $e4;

                        $user->name = $e1 . $e2 . $e3 . $e4;
                        $user->surname = $e1 . $e2 . $e3 . $e4;
                		$user->patronymic = $e1 . $e2 . $e3 . $e4;

                		$user->email = $e1 . $e2 . $e3 . $e4 . '@mail.com';
                		$user->password = \Hash::make("qweqweqwe");
                		$user->phone = "111111111111111111111";
                		$user->bank_account = "111111111111111111111";
                		$user->bank_name = "111111111111111111111";
                		$user->bik = "111111111111111111111";

                		$user->birth_date = date("11-11-1981");
                		$user->address = "111111111111111111111";
                		$user->active = 1;
                		$user->cycle = 1;

                        $user_me = ClubUser::find(2);
                        $parent_user = ClubUser::getFairChild($user_me);

                        $user->level = $parent_user->level + 1;
                        $user->parent_id = $parent_user->id;
                        $user->encourager_id = $user_me->id;

                        $user->position = $parent_user->children->count() + 1;

                        $user->avatar = '/images/user.png';

                		$user->save();

                        ClubUser::getTwentyOne($user->id);
                    }
                }
            }
        }
    }
}

class HouseUsersTableSeeder extends Seeder {

    public function run()
    {
        Model::unguard();
        $arr = ['E','F','G'];
        foreach ($arr as $e1){
            foreach ($arr as $e2){
                foreach ($arr as $e3){
                    foreach ($arr as $e4){
                		$user = new HouseUser();

                		$user->login = $e1 . $e2 . $e3 . $e4;

                        $user->name = $e1 . $e2 . $e3 . $e4;
                        $user->surname = $e1 . $e2 . $e3 . $e4;
                		$user->patronymic = $e1 . $e2 . $e3 . $e4;

                		$user->email = $e1 . $e2 . $e3 . $e4 . '@mail.com';
                		$user->password = \Hash::make("qweqweqwe");
                		$user->phone = "111111111111111111111";
                		$user->bank_account = "111111111111111111111";
                		$user->bank_name = "111111111111111111111";
                		$user->bik = "111111111111111111111";

                		$user->birth_date = date("11-11-1981");
                		$user->address = "111111111111111111111";
                		$user->active = 1;
                		$user->cycle = 1;

                        $user_me = HouseUser::find(89);
                        $parent_user = HouseUser::getFairChild($user_me);

                        $user->level = $parent_user->level + 1;
                        $user->parent_id = $parent_user->id;
                        $user->encourager_id = $user_me->id;

                        $user->position = $parent_user->children->count() + 1;

                        $user->avatar = '/images/user.png';

                		$user->save();

                    }
                }
            }
        }
    }
}
