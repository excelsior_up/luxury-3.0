<?php

return [

	'multi' => [
        'club_user' => [
            'driver' => 'eloquent',
            'model' => 'App\ClubUser',
        ],
        'house_user' => [
            'driver' => 'eloquent',
            'model' => 'App\HouseUser',
            'table' => 'house_users'
        ],
        'admin' => [
            'driver' => 'eloquent',
            'model' => 'App\Admin',
        ],
        'manager' => [
        	'driver' => 'eloquent',
        	'model' => 'App\Manager',
        ],
    ],

    'password' => [
        'email' => 'emails.password',
        'table' => 'password_resets',
        'expire' => 60,
	],

];
