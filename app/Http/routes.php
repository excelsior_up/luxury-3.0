<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::post('requests', 'UserRequestController@store');

Route::controller('/user_request', 'UserRequestController');

Route::group(['prefix' => 'club'], function() {

    Route::controller('/', 'ClubController');

});

Route::group(['prefix' => 'house'], function() {

    Route::controller('/', 'HouseController');

});

Route::controller('/admin', 'AdminController');

Route::controller('/', 'MainController');
