<?php 

/**
 * redirect if necessary user to the next cycle
 * 
 */
function manageCycles($user)
{
	$user_parent = App\User::where("parent_id", $user->parent_id)->where("active", 1)->get();
	
	$users = App\User::find($user->parent_id);
	if($user_parent->count() % 12 == 0 && $user_parent->count())
	{
		$users->cycle++;
		if($users->cycle <= 5){
			$users->save();	
		}
	}
}

/**
 * Check all users in all cycles for complicity of their children (== 12)
 * Then refinance_count++
 */
function refinance()
{
	$users = App\User::all();

	foreach ($users as $key => $user) {
		$doRefinance = true;

		foreach ($user->children as $key => $child) {
			if ($child->refinance_count == $user->refinance_count)
			{
				foreach ($child->children as $key => $child_child) {
					if ($child_child->refinance_count == $user->refinance_count)
					{}
					else
					{
						$doRefinance = false;
						break;
					}
				}
			}
			else
			{
				$doRefinance = false;
				break;
			}
		}

		if ($doRefinance)
		{
			$user->refinance_count++;
			$user->save();
		}
	}
}

