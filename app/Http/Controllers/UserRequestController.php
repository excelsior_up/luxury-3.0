<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
//use Request;
use App\UserRequest;
use Validator;

// Для сохранения файлов
use Storage;
use Input;
use Auth;

// Для отправки почты
use Mail;

// Для перенаправления назад
use Redirect;

class UserRequestController extends Controller {

	public function __construct()
	{
		$this->middleware('auth.admin', [
			'only' => [
				'getDownload', 
			]
		]);
	}
	
	/*
	public function index()
	{
		//
	}

	public function create()
	{
		//
	}
	 */
	 
	public function store(Request $request)
	{
		$messages = array(
			'sender_text.required'      => 'Текст заявки обязателен.',
			'sender_file_name.max' 		=> 'Размер файла не должен превышать 3 Мб',
		);

		$rules = array(
			'sender_text'      => 'required',
			'sender_file_name' => 'max:3000',
		);

		$this->validate($request, $rules, $messages);
		
		$current_user = Auth::user()->get();
		
		$rq = new UserRequest;
		$rq->name  = $current_user->name . " " . $current_user->surname;
		$rq->email = $current_user->email;
		$rq->text  = $request->input('sender_text');
		
		if ($request->hasFile($request->get('sender_file_name')))
		{   
			$rq->file_name = Input::file('sender_file_name')->getClientOriginalName();
			
			Storage::put(
				sprintf("request__%s__%s__%s", 
					$rq->name,   
					\Carbon\Carbon::now(),
					$rq->file_name   
				),
				file_get_contents($request->file('sender_file_name')->getRealPath())
			);
			
			$rq->has_file = true;
		}
		else
		{
			$rq->file_name = '';
			$rq->has_file = false;
		}

		$rq->is_read = false;
		$rq->save();
		
		Mail::send('emails.mail_notification', ['user' => Auth::user(), 'request' => $rq], function($message) {
		 	$message->to(['info@luxurylife', 'zhandos0176@gmail.com', 'zzmj95@gmail.com'])->subject('Новая заявка на сайте luxurylife.us');    
		});
				
		return redirect('/user/contacts');
	}

	/*
	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		//
	}


	public function update($id)
	{
		//
	}

	public function destroy($id)
	{
		//
	}
	*/
	
	public function getDownload($id)
	{
		$rq = UserRequest::findOrFail($id);
		
		$file = sprintf("request__%s__%s__%s", 
					$rq->name,   
					$rq->created_at,
					$rq->file_name 
				);
		
		$dirFile = base_path() . '/storage/app/' . $file;
				
		return response()->download($dirFile);
	}
	
	public function getChecked($id)
	{
		$rq = UserRequest::findOrFail($id);
		
		if ($rq->is_read)
		{
			$rq->is_read = false;
		}
		else
		{
			$rq->is_read = true;
		}
		$rq->save();
		return Redirect::back();
	}
}