<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth, App, Validator, Config, Input, Hash, App\ClubUser, App\Post, Redirect, Request, Mail, Session;
use App\ClubUser2;
use App\HouseUser;

class ClubController extends Controller {

	public function __construct()
	{
		$this->middleware('auth.club', ['except' => 'postLogin']);
	}

	public function getIndex()
	{
		$user = Auth::club_user()->get();

		$roman = ["I", "II", "III", "IV", "V"];

		return view("user.profile", compact('user', 'roman'));
	}

	public function getStructure()
	{
		$user = Auth::club_user()->get();
        $user2 = ClubUser2::where('login', $user->login)->first();

        return view('user.team', compact('user', 'user2'));
	}

	public function getMyStructure()
	{
		$user  = Auth::club_user()->get();
        $user2 = ClubUser2::where('login', $user->login)->first();

		return view('user.my_structure',
			[
             'user2' => $user2,
			 'user'  => $user
			]);
	}

	public function getRules()
	{
		$user = Auth::club_user()->get();

		return view("shared.rules",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

	public function getMarketing()
	{
		$user = Auth::club_user()->get();

		return view("shared.marketing",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

	public function getNews()
	{
		$user = Auth::club_user()->get();

		$posts = Post::orderBy('created_at', 'DESC')->get();

		return view("user.news",
			[
				'user' => $user,
				'posts' => $posts
			]
		);
	}

	public function getNewsPage($id)
	{
		$post = Post::find($id);
		$user = Auth::club_user()->get();
		if (!$post)
			App::abort(404);

		return view("user.news_page",
			[
				'user' => $user,
				'post' => $post
			]
		);
	}

	public function getContacts()
	{
		$user = Auth::club_user()->get();
		return view("shared.contacts",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

	public function getEditProfile()
	{
		$user = Auth::club_user()->get();

		return view("user.edit_profile",
			[
				'user' => $user
			]
		);
	}

	public function postEditProfile()
	{
		$users = ClubUser::find(Auth::club_user()->get()->id);

		if(Request::input("old_password") != "")
		{
		  	if (Hash::check(Request::input("old_password"), Auth::club_user()->get()->password))
		  	{
		  		$users->password = Hash::make(Request::input("new_password"));
		  	}
		  	else
		  	{
		  		return Redirect::to("/club/edit-profile")->with("message","Пароль неверный");
		  	}
		}

		$users->name = Request::input("name");
		$users->surname = Request::input("surname");
		$users->patronymic = Request::input("patronymic");
		$users->phone = Request::input("phone");
		$users->bank_account = Request::input("bank_account");
		$users->bank_name = Request::input("bank_name");
		$users->bik = Request::input("bik");
		$users->card_id = Request::input("card_id");
		$users->email = Request::input("email");

		$users->birth_date = date('Y.m.d', strtotime(Request::input("birth_date")))	;
		$users->address = Request::input("address");
		$users->save();

		//return Redirect::to("/club/edit-profile");
        return Redirect::to("/club");
	}

	public function postChangeAvatar()
	{
		if (Request::hasFile('avatar'))
		{
			$user = Auth::club_user()->get();
			$dir = '/users/avatars/';
			$destinationPath = \Config::get('app.img_path') . $dir;

			if(!strpos($user->avatar, 'user.png')){
				$arr = explode('/',$user->avatar);
				unlink(public_path() ."/uploads". $dir . $arr[6]);
			}

			$file = Request::file('avatar');
			$filename = str_random(40);

			while(file_exists($destinationPath.$filename.'.jpg'))
				$filename = str_random(40);

			$upload_success = $file->move($destinationPath, $filename. '.jpg');

			if ($upload_success)
			{
				try
				{
					/*$img = \Image::make($destinationPath.$filename. '.jpg');
					$img->resize(400, null, function ($constraint)
						{
							$constraint->aspectRatio();
							$constraint->upsize();
						}
					);
					$img->save($destinationPath.$filename.'.jpg');*/

					$user->avatar = \Config::get('app.img_url') . $dir . $filename.'.jpg';
					$user->save();
				}
				catch(Exception $e)
				{
					//Log::alert("Invalid photo [".$destinationPath.$filename."] image type [".$file->getClientOriginalExtension()."] by company [".$company->id."].");
				}

			}
		}

	}

	public function postChangePassword()
	{
		$user = Auth::club_user()->get();
		$old_password = Request::input("old_password");
		$new_password = Request::input("new_password");
		if(\Hash::check($old_password, $user->password)){
			$user->password = \Hash::make($new_password);
			$user->save();
		}
		else
		{
			return Redirect::back()->with("edit_message","Неправильный старый пароль");
		}


		return Redirect::back()->with("edit_message","Вы успешно поменяли пароль");
	}

	public function postLogin()
	{
		$login = Request::input('login');
		$password = Request::input('password');
        $user = ClubUser::where('login', '=', $login)->first();

        if(! $user)
        {
            return Redirect::back()->withInput()->with('error-message', 'Пользователя с таким логином не существует');
        }

        if($user->PIN != NULL)
        {
            $user->hasPayed = NULL;
            $user->save();
            if (
                Auth::club_user()->attempt(['login' => $login, 'password' => $password, 'hasPayed' => NULL])
            )
            {
                return Redirect::to("/club");
            }
            else
            {
                return Redirect::back()->withInput()->with('error-message', 'Неправильный пароль');
            }
        }
        else
        {
            return Redirect::back()->withInput()->with('error-message', 'Вы не внесли оплату');
        }

        /*
        if (Auth::club_user()->attempt(['login' => $login, 'password' => $password, 'hasPayed' => NULL]))
        {
            return Redirect::to("/club");
        }
        else
        {
            return Redirect::back()->withInput()->with('error-message', 'Неправильные данные для входа или вы не внесли оплату');
        }



		if (Request::has('pin'))
		{
			if (Auth::club_user()->attempt(['login' => $login, 'password' => $password, 'PIN' => Request::input('pin')]))
			{
				$user = Auth::club_user()->get();

                if($user->hasPayed != NULL)
				{
					$user->hasPayed = NULL;
					$user->activated_at = date('Y-m-d H:i:s');
				}

				$user->save();

				return Redirect::to("/club");
			}

		}
		else
		{}
        */
	}

	public function getOneStructure()
 	{
		$id = Request::input("id");
 		if (is_numeric($id))
 		{
 			$user = Auth::club_user()->get();

 			$structure = ClubUser::where("parent_id", $id)->where("active", 1);

 			return view('user.subscriber',['structure'=> $structure, 'user'=>$user]);
 		}

		exit;
 	}

 	public function getRegister()
	{
		$user = Auth::club_user()->get();

		return view("user.register",
			[
				'user' => $user
			]
		);
	}

	public function postRegister()
	{
		$user_me = Auth::club_user()->get();

		$validation = \Validator::make(
			[
				'login' => Request::input('login'),
                'email' => Request::input('email')
			],
			[
				'login' => 'unique:club_users',
                'email' => 'unique:club_users'
			]
		);

		if ($validation->fails())
		{
			return redirect()->back()->withInput()->with('email-error', 'Такой e-mail или логин уже существует');
		}

		$user = new ClubUser();

		$user->login = Request::input("login");

        $user->name = Request::input("name");
        $user->surname = Request::input("surname");
		$user->patronymic = Request::input("patronymic");

		$user->email = Request::input("email");
		$user->password = \Hash::make(Request::input("password"));
		$user->phone = Request::input("phone");
		$user->bank_account = Request::input("bank_account");
		$user->bank_name = Request::input("bank_name");
		$user->bik = Request::input("bik");

		$user->birth_date = date('Y.m.d', strtotime(Request::input("birth_date")));
		$user->address = Request::input("address");
		$user->active = 1;
        $user->hasPayed = 'no';
		$user->cycle = 1;

        $parent_user = ClubUser::getFairChild($user_me);

        $user->level = $parent_user->level + 1;
        $user->parent_id = $parent_user->id;
        $user->encourager_id = $user_me->id;

        $user->position = $parent_user->children->count() + 1;

        $user->avatar = '/images/user.png';

		$user->save();

        // вот она - проверка на переход во 2 цикл
        ClubUser::getTwentyOne($user->id);

		// send email to new registered user about link to login and information, like go and pay 25E

		/*Mail::send('emails.user.registered_under_user', [], function($message) use($user) {
			$message->to($user->email)->subject('Оплатите регистрацию');
		});*/

		return Redirect::to("/club/structure");
	}

    /****
     *  Метод добавления авторизированного пользователя клубной
     *  программы в жилищную программу.
     */

/*

        public function getToHouseProgram()
        {
            // текущий пользователь
            $user = Auth::club_user()->get();

            // текущий пользователь уже имеется в жилищной программе?
            $checking_house_user = HouseUser::where('login', $user->login)
                ->where('email', $user->email)->first();
            if ($checking_house_user != null)
            {
                Session::flash('home_message', 'Вы уже находитесь в жилищной программе!
                    Перезайдите в аккаунт с параметром "Жилищная программа".');
                Session::flash('home_message_type', 'error');
                return redirect()->to("/club");
            }
            else
            {
                // добавление пользователя клубной программы в жилищную программу
                $house_user = new HouseUser();

                $house_user->login        = $user->login;

                $house_user->name         = $user->name;
                $house_user->surname      = $user->surname;
                $house_user->patronymic   = $user->patronymic;

                $house_user->email        = $user->email;
                $house_user->password     = $user->password;
                $house_user->phone        = $user->phone;
                $house_user->bank_account = $user->bank_account;
                $house_user->bank_name    = $user->bank_name;
                $house_user->bik          = $user->bik;

                $house_user->birth_date   = $user->birth_date;
                $house_user->address      = $user->address;
                $house_user->active       = 1;
                $house_user->cycle        = $user->cycle;

                $user_me = HouseUser::find(1);
                if($user_me){
                    $parent_user = HouseUser::getFairChild($user_me);
                    $house_user->level         = $parent_user->level + 1;
                    $house_user->parent_id     = $parent_user->id;
                    // rechecking ...
                    $house_user->encourager_id = $user_me->id;
                    $house_user->position      = $parent_user->children->count() + 1;
                } else {
                    $house_user->level         = 0;
                    $house_user->parent_id     = 0;
                    $house_user->encourager_id = 0;
                    $house_user->position      = 1;
                }

                $house_user->avatar = '/images/user.png';
                $house_user->save();

                Session::flash('home_message', 'Отлично! Вы только что вошли в жилищную программу!
                    Перезайдите в аккаунт с параметром "Жилищная программа".');
                Session::flash('home_message_type', 'success');
                return redirect()->to('/club');
            }
        }

*/

	public function getGenerateLink()
	{
		$user = Auth::club_user()->get();

		$link_pre = str_random(5);
		$link_post = str_random(5);

		/*$validation = Validator::make(
			['link' => $link_pre.$link_post],
			['link' => 'unique:user_links']
		);

		while($validation->fails())
		{
			$link_pre = str_random(5);
			$link_post = str_random(5);

			$validation = Validator::make(
				['link' => $link_pre.$link_post],
				['link' => 'unique:user_links']
			);
		}*/

		$link = $link_pre . '.' . $user->id . '.' . $link_post;

		$userlink = new App\UserLink;
		$userlink->user_id = $user->id;
        $userlink->user_type = 'club';
		$userlink->link = $link;
		$userlink->save();

		echo 'http://luxurylife.us/signup?from=invitation&token=' . $link;

		exit;
	}

	public function getLogout()
	{
		Auth::club_user()->logout();
		return redirect()->to("/");
	}

	public function getRulesAgreed()
	{
		$user = Auth::club_user()->get();

	 	return view("shared.pre-auth-rules",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

	public function postEmailSend()
	{
		$user = Auth::club_user()->get();
		$email = Request::input("link_email_send");
		$full_message = Request::input("link_message_send");
		$link = Request::input("link_send");

		Mail::send('emails.send_link', ['user' => $user, 'link' => $link, 'full_message' => $full_message], function($message) use ($email) {
			$message->to($email)->subject('Регистрация на сайте luxurylife.us');
		});


		return Redirect::back()->with('success', 'Ссылка отправлена на почту');
	}

}
