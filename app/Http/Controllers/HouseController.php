<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth, App, Validator, Config, Input, Hash, App\ClubUser, App\Post, Redirect, Request, Mail, Session;

use App\HouseUser;

class HouseController extends Controller {

    public function __construct()
	{
		$this->middleware('auth.house', ['except' => 'postLogin']);
	}

    public function getIndex()
    {
        $user = Auth::house_user()->get();

		$roman = ["I", "II", "III", "IV", "V"];

		return view("house_user.profile", compact('user', 'roman'));
    }

    public function getStructure()
	{
		$user = Auth::house_user()->get();
        $counter = 2; // Ветвление в списке

        return view('house_user.team', compact('user', 'counter'));
	}

    public function getMyStructure()
	{
		$user  = Auth::house_user()->get();

		return view('house_user.my_structure',
			[
			 'user'  => $user
			]);
	}

    public function getRules()
	{
		$user = Auth::house_user()->get();

		return view("shared.rules",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

    public function getMarketing()
	{
		$user = Auth::house_user()->get();

		return view("shared.marketing",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

    public function getNews()
	{
		$user = Auth::house_user()->get();

		$posts = Post::orderBy('created_at', 'DESC')->get();

		return view("user.news",
			[
				'user' => $user,
				'posts' => $posts
			]
		);
	}

    public function getNewsPage($id)
	{
		$post = Post::find($id);
		$user = Auth::house_user()->get();
		if (!$post)
			App::abort(404);

		return view("user.news_page",
			[
				'user' => $user,
				'post' => $post
			]
		);
	}

    public function getContacts()
	{
		$user = Auth::house_user()->get();
		return view("shared.contacts",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

    public function getEditProfile()
	{
		$user = Auth::house_user()->get();

		return view("house_user.edit_profile",
			[
				'user' => $user
			]
		);
	}

    public function postEditProfile()
	{
		$users = HouseUser::find(Auth::house_user()->get()->id);

		if(Request::input("old_password") != "")
		{
		  	if (Hash::check(Request::input("old_password"), Auth::house_user()->get()->password))
		  	{
		  		$users->password = Hash::make(Request::input("new_password"));
		  	}
		  	else
		  	{
		  		return Redirect::to("/house/edit-profile")->with("message","Пароль неверный");
		  	}
		}

		$users->name = Request::input("name");
		$users->surname = Request::input("surname");
		$users->patronymic = Request::input("patronymic");
		$users->phone = Request::input("phone");
		$users->bank_account = Request::input("bank_account");
		$users->bank_name = Request::input("bank_name");
		$users->bik = Request::input("bik");
		$users->card_id = Request::input("card_id");
		$users->email = Request::input("email");

		$users->birth_date = date('Y.m.d', strtotime(Request::input("birth_date")))	;
		$users->address = Request::input("address");
		$users->save();

        return Redirect::to("/house");
	}

    public function postChangeAvatar()
	{
		if (Request::hasFile('avatar'))
		{
			$user = Auth::house_user()->get();
			$dir = '/users/avatars/';
			$destinationPath = \Config::get('app.img_path') . $dir;

			if(!strpos($user->avatar, 'user.png')){
				$arr = explode('/',$user->avatar);
				unlink(public_path() ."/uploads". $dir . $arr[6]);
			}

			$file = Request::file('avatar');
			$filename = str_random(40);

			while(file_exists($destinationPath.$filename.'.jpg'))
				$filename = str_random(40);

			$upload_success = $file->move($destinationPath, $filename. '.jpg');

			if ($upload_success)
			{
				try
				{
					// $user->avatar = \Config::get('app.img_url') . $dir . $filename.'.jpg';
                    $user->avatar = "/uploads" . $dir . $filename.'.jpg';
					$user->save();
				}
				catch(Exception $e)
				{
					//Log::alert("Invalid photo [".$destinationPath.$filename."] image type [".$file->getClientOriginalExtension()."] by company [".$company->id."].");
				}
			}
		}
	}

    public function postChangePassword()
	{
		$user = Auth::house_user()->get();
		$old_password = Request::input("old_password");
		$new_password = Request::input("new_password");
		if(\Hash::check($old_password, $user->password)){
			$user->password = \Hash::make($new_password);
			$user->save();
		}
		else
		{
			return Redirect::back()->with("edit_message","Неправильный старый пароль");
		}


		return Redirect::back()->with("edit_message","Вы успешно поменяли пароль");
	}

    public function postLogin()
	{
        $login = Request::input('login');
		$password = Request::input('password');
        $user = HouseUser::where('login', '=', $login)->first();

        if(! $user)
        {
            return Redirect::back()->withInput()->with('error-message', 'Пользователя с таким логином не существует');
        }

        if($user->PIN != NULL)
        {
            $user->hasPayed = NULL;
            $user->save();
            if (
                Auth::house_user()->attempt(['login' => $login, 'password' => $password, 'hasPayed' => NULL])
            )
            {
                return Redirect::to("/house");
            }
            else
            {
                return Redirect::back()->withInput()->with('error-message', 'Неправильный пароль');
            }
        }
        else
        {
            return Redirect::back()->withInput()->with('error-message', 'Вы не внесли оплату');
        }
	}

    public function getOneStructure()
 	{
		$id = Request::input("id");
 		if (is_numeric($id))
 		{
 			$user = Auth::house_user()->get();

 			$structure = HouseUser::where("parent_id", $id)->where("active", 1);

 			return view('house_user.subscriber',['structure'=> $structure, 'user'=>$user]);
 		}

		exit;
 	}

    public function getRegister()
	{
		$user = Auth::house_user()->get();

		return view("house_user.register",
			[
				'user' => $user
			]
		);
	}

    public function postRegister()
	{
		$user_me = Auth::house_user()->get();

		$validation = \Validator::make(
			[
				'login' => Request::input('login'),
                'email' => Request::input('email')
			],
			[
				'login' => 'unique:house_users',
                'email' => 'unique:house_users'
			]
		);

		if ($validation->fails())
		{
			return redirect()->back()->withInput()->with('email-error', 'Такой e-mail или логин уже существует');
		}

		$user = new HouseUser();

		$user->login = Request::input("login");

        $user->name = Request::input("name");
        $user->surname = Request::input("surname");
		$user->patronymic = Request::input("patronymic");

		$user->email = Request::input("email");
		$user->password = \Hash::make(Request::input("password"));
		$user->phone = Request::input("phone");
		$user->bank_account = Request::input("bank_account");
		$user->bank_name = Request::input("bank_name");
		$user->bik = Request::input("bik");

		$user->birth_date = date('Y.m.d', strtotime(Request::input("birth_date")));
		$user->address = Request::input("address");
		$user->active = 1;
		$user->hasPayed = 'no';
		$user->cycle = 1;

        $parent_user = HouseUser::getFairChild($user_me);
        // $parent_user = HouseUser::getFairChild($user_me);

        $user->level = $parent_user->level + 1;
        $user->parent_id = $parent_user->id;
        $user->encourager_id = $user_me->id;

        $user->position = $parent_user->children->count() + 1;

        $user->avatar = '/images/user.png';

		$user->save();

		return Redirect::to("/house/structure");
	}

    public function getGenerateLink()
	{
		$user = Auth::house_user()->get();

		$link_pre = str_random(5);
		$link_post = str_random(5);

		$link = $link_pre . '.' . $user->id . '.' . $link_post;

		$userlink = new App\UserLink;
		$userlink->user_id = $user->id;
        $userlink->user_type = 'house';
		$userlink->link = $link;
		$userlink->save();

		echo 'http://luxurylife.us/signup?from=invitation&token=' . $link;

		exit;
	}

    public function getLogout()
	{
		Auth::house_user()->logout();
		return redirect()->to("/");
	}

	public function getRulesAgreed()
	{
		$user = Auth::house_user()->get();

	 	return view("shared.pre-auth-rules",
			[
				'user' => $user,
				'extends' => 'registr'
			]
		);
	}

    public function postEmailSend()
	{
		$user = Auth::house_user()->get();
		$email = Request::input("link_email_send");
		$full_message = Request::input("link_message_send");
		$link = Request::input("link_send");

		Mail::send('emails.send_link', ['user' => $user, 'link' => $link, 'full_message' => $full_message], function($message) use ($email) {
			$message->to($email)->subject('Регистрация на сайте luxurylife.us');
		});


		return Redirect::back()->with('success', 'Ссылка отправлена на почту');
	}

}
