<?php

namespace App\Http\Controllers;

use Mail;

use Auth, App, Validator, Config, Input, Hash,
    App\ClubUser, App\PasswordReset, Redirect,
    Request, Session;
use App\HouseUser;

class MainController extends Controller {

	public function getIndex()
	{
		return view('landing.land');
	}

	public function getSignin()
	{
		return view('pre-auth.login', ['entity' => 'user']);
	}

	public function getSignup()
	{
		return view('pre-auth.registration');
	}

    public function postSignup()
    {
        if (Request::has('token'))
        {
            $token = explode('.', Request::input('token'));

            $selectingToken = App\UserLink::where('link', Request::input('token'))
                ->where('hasUsed', 0)->first();
            // Проверка на существование ссылки
            if ($selectingToken->count())
            {
                // Существует
                // ...
                // Тогда проверка на тип ссылки (жилищная / клубная)

                if ($selectingToken->user_type == "house")
                {
                    $user = new HouseUser();

                    $validation = \Validator::make(
                        [
                            'login' => Request::input('login'),
                            'email' => Request::input('email')
                        ],
                        [
                            'login' => 'unique:house_users',
                            'email' => 'unique:house_users'
                        ]
                    );

                    if ($validation->fails())
                    {
                        return redirect()->back()->withInput()->with('error', 'Такой e-mail или логин уже существует.');
                    }

                    $user->login = Request::input("login");
                    $user->name = Request::input("name");
                    $user->email = Request::input("email");
                    $user->password = \Hash::make(Request::input("password"));
                    $user->phone = Request::input("phone");
                    $user->bank_account = Request::input("bank_account");
                    $user->bank_name = Request::input("bank_name");
                    $user->bik = Request::input("bik");
                    $user->address = Request::input("address");
                    $user->active = 1; // has Payed 25 euro ?
                    $user->hasPayed = 'no';
                    $user->cycle = 1;

                    $user_id = $token[1];

                    $link_owner = HouseUser::find($user_id);

                    $parent_user = HouseUser::getFairChild($link_owner);

                    $user->parent_id = $parent_user->id;
                    $user->encourager_id = $user_id;
                    $user->level = $parent_user->level + 1;

                    $user->position = $parent_user->children->count() + 1;

                    $selectingToken->hasUsed = 1;
                    $selectingToken->save();

                    $user->avatar = '/images/user.png';
                    $user->save();
                    return Redirect::to("/post-registration");
                }
                else {
                    if ($selectingToken->user_type == "club")
                    {
                        $user = new ClubUser();

                        $validation = \Validator::make(
                            [
                                'login' => Request::input('login'),
                                'email' => Request::input('email')
                            ],
                            [
                                'login' => 'unique:club_users',
                                'email' => 'unique:club_users'
                            ]
                        );

                        if ($validation->fails())
                        {
                            return redirect()->back()->withInput()->with('error', 'Такой e-mail или логин уже существует.');
                        }

                        $user->login = Request::input("login");
                        $user->name = Request::input("name");
                        $user->email = Request::input("email");
                        $user->password = \Hash::make(Request::input("password"));
                        $user->phone = Request::input("phone");
                        $user->bank_account = Request::input("bank_account");
                        $user->bank_name = Request::input("bank_name");
                        $user->bik = Request::input("bik");
                        $user->address = Request::input("address");
                        $user->active = 1; // has Payed 25 euro ?
                        $user->hasPayed = 'no';
                        $user->cycle = 1;

                        $user_id = $token[1];

                        $link_owner = ClubUser::find($user_id);

                        $parent_user = ClubUser::getFairChild($link_owner);

                        $user->parent_id = $parent_user->id;
                        $user->encourager_id = $user_id;
                        $user->level = $parent_user->level + 1;

                        $user->position = $parent_user->children->count() + 1;

                        $selectingToken->hasUsed = 1;
                        $selectingToken->save();

                        $user->avatar = '/images/user.png';
                        $user->save();

                        ClubUser::getTwentyOne($user->id);

                        return Redirect::to("/post-registration");
                    }
                    else {
                        return redirect()->back()->withInput()->with('token-error', '!!!');
                    }

                }

            }
            else
            {
                // Ошибка
                return redirect()->back()->withInput()->with('token-error', 'Неправильная ссылка. Пожалуйста, проверьте ссылку и попробуйте еще раз');
            }
        }
        else
        {
            $user = new ClubUser();

            $validation = \Validator::make(
                [
                    'login' => Request::input('login'),
                    'email' => Request::input('email')
                ],
                [
                    'login' => 'unique:club_users',
                    'email' => 'unique:club_users'
                ]
            );

            if ($validation->fails())
            {
                return redirect()->back()->withInput()->with('error', 'Такой e-mail или логин уже существует.');
            }

            $user->login = Request::input("login");
            $user->name = Request::input("name");
            $user->email = Request::input("email");
            $user->password = \Hash::make(Request::input("password"));
            $user->phone = Request::input("phone");
            $user->bank_account = Request::input("bank_account");
            $user->bank_name = Request::input("bank_name");
            $user->bik = Request::input("bik");
            $user->address = Request::input("address");
            $user->active = 1; // has Payed 25 euro ?
            $user->hasPayed = 'no';
            $user->cycle = 1;

            $parent_user = ClubUser::getFairPosition();

            if(Request::input('loginsponsor'))
            {
                $enqourager_user = ClubUser::where(
                    'login', '=', Request::input('loginsponsor')
                )->first();
                if ($enqourager_user != NULL)
                {
                    $parent_user = ClubUser::getFairChild($enqourager_user);
                    $user->encourager_id = $enqourager_user->id;
                    //
                    // Session::flash('error_sponsor', 'yes');
                    // return redirect()->back()->withInput();
                }
                else
                {
                    Session::flash('error_sponsor', 'Пользователя с таким логином не существует');
                    return redirect()->back()->withInput();
                }
            }
            else
            {
                $parent_user = ClubUser::getFairPosition();
            }


            if ($parent_user)
            {
                $user->position = $parent_user->children->count() + 1;
                $user->parent_id = $parent_user->id;
                //$user->encourager_id = $parent_user->id;
                $user->level = $parent_user->level + 1;
            }
            else
            {
                $user->position = 0;
                $user->parent_id = 0;
                $user->encourager_id = 1;
                $user->level = 0;
            }

            $user->avatar = '/images/user.png';

            $user->save();

            ClubUser::getTwentyOne($user->id); // Проверка на переход (2 цикл)

            return Redirect::to("/post-registration");

        }
    }

	public function getReset()
	{
		return view('pre-auth.password_reset', ['entity' => 'user']);
	}

	public function getChangePassword()
	{
		if(Request::has('token')){

			$pass_reset = PasswordReset::where('token', Request::input("token"))->first();
			$email = null;
			$token_send = Request::input("token");
			if(!$pass_reset or $pass_reset->active == 0)
			{
				\Session::set('error', "Неправильная ссылка");
			}
			else
			{
				$time = $pass_reset->created_at;

				$hour = date("H", strtotime($time));
				$minute = date("i", strtotime($time)) + $hour*60;
				$date = date("Y:m:d", strtotime($time));


				$now_date = date("Y:m:d");
				$now_minute = date('i')+ date('H')*60;
				$email = $pass_reset->email;

				if($now_date != $date or $now_minute-$minute > 60)
				{
					\Session::set('error', "Это ссылка устарела");

				}
			}
		}else{
			return Redirect::to("/reset");
		}


		\Session::set('reset_email', $email);
		\Session::set('token_send', $token_send);

		return view("pre-auth.reset_link", ['entity' => 'admin'] );

	}

	public function postChangePassword()
	{
		if(\Session::has("reset_email") and \Session::has("token_send"))
		{
			$email = \Session::get("reset_email");
			$token_send = \Session::get("token_send");
		}
		else
		{
			return Redirect::back()->with("error", "Неправильная ссылка");
		}

		$password = Request::input("password");
		$user = User::where("email",$email)->first();

		$user->password = \Hash::make($password);
		$user->save();


		\DB::table('password_resets')
            ->where('token', $token_send)
            ->update(['active' => 0]);


		return Redirect::back()->with("success", "Вы успешно поменяли пароль");
	}

	public function postReset()
	{
		$email = Request::input("email");

    $ifClubExists = ClubUser::where('email', $email)->first();
    $ifHouseExists = HouseUser::where('email', $email)->first();

    if(($ifClubExists != null) || ($ifHouseExists != null))
    {
      $email_exist = ($ifClubExists != null ? $ifClubExists : $ifHouseExists);
    }
    else{
	       return Redirect::back()->with("error", "Данный email не найден");
    }

			$token = str_random(30);

			$validation = Validator::make(
				['token' => $token],
				['token' => 'unique:password_resets']
			);

			while ($validation->fails())
			{
				$token = str_random(30);

				$validation = Validator::make(
					['token' => $token],
					['token' => 'unique:password_resets']
				);
			}
			$password_resets = new PasswordReset;
			$password_resets->email = $email;
			$password_resets->token = $token;
			$password_resets->save();
			$url = \Config::get('app.url');
			$link = $url.'/change-password?token='.$token;
			Mail::send('emails.password_remind', ['user' => $email_exist, 'link' => $link], function($message) use ($email) {
				$message->to($email)->subject('Восстановление пароля на сайте luxurylife.us');
			});


		return Redirect::to("after-reset");
	}

	public function getAfterReset(){

		return view("pre-auth.after_reset");
	}

	public function getPostRegistration()
	{
		return view('pre-auth.post-register');
	}

	public function getRulesAgreed()
	{
		return view('pre-auth.auth_rules');
	}

	public function getClubRules()
	{
		return response()->download(public_path() . '/docs/Pravila_cluba_pravovoj_zaschity.docx');
	}

}
