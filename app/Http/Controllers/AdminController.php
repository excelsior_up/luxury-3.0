<?php

namespace App\Http\Controllers;

use App\Http\Requests;
//use App\Http\Controllers\Controller;
use Auth, App, Validator, Config, Input, Hash, App\ClubUser, App\HouseUser, App\Post, App\Admin, App\PasswordReset, Redirect, Request;
use Mail;

//Для заявок
use App\UserRequest;

class AdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth.admin', [
			'except' => [
				'getLogin',
				'postLogin',
				'getReset',
				'postReset',
				'getMakePassword',
				'postMakePassword'
			]
		]);
	}

	public function getProfile()
	{
		$admin = Auth::admin()->get();

		return view("admin.profile", ['user' => $admin]);
	}

	public function getStructure()
	{
		$admin = Auth::admin()->get();

		return view("admin.structure", ['user' => $admin]);
	}

	public function getRules()
	{
		$admin = Auth::admin()->get();

		return view("shared.rules", ['user' => $admin, 'extends' => 'admin']);
	}

	public function getMarketing()
	{
		$admin = Auth::admin()->get();

		return view("shared.marketing", ['user' => $admin, 'extends' => 'admin']);
	}

	public function postRegister()
	{
		$user_me = Auth::user()->get();

		$validation = \Validator::make(
			[
				'email' => Request::input("email"),
				'login' => Request::input('login')
			],
			[
				'email' => 'unique:users',
				'login' => 'unique:users'
			]
		);

		if ($validation->fails())
		{
			return redirect()->back()->withInput()->with('email-error', 'Такой e-mail или логин уже существует');
		}

		$user = new User();

		$user->login = Request::input("login");
		$user->name = Request::input("name");
		$user->surname = Request::input("surname");
		$user->patronymic = Request::input("patronymic");
		$user->email = Request::input("email");
		$user->password = \Hash::make(Request::input("password"));
		$user->phone = Request::input("phone");
		$user->bank_account = Request::input("bank_account");
		$user->bank_name = Request::input("bank_name");
		$user->bik = Request::input("bik");

		$parent = $user->getFairChild($user_me);

		$user->birth_date = date(Request::input("birth_date"));
		$user->level = -1;
		$user->address = Request::input("address");
		$user->parent_id = $parent->id;
		$user->active = 0;
		$user->cycle = 1;
		$user->avatar = "/images/user.png";

		$user->save();

        // вот она - проверка на переход во 2 цикл
        ClubUser::getTwentyOne($user->id);

		// send email to new registered user about link to login and information, like go and pay 25E

		/*Mail::send('emails.user.registered_under_user', [], function($message) use($user) {
			$message->to($user->email)->subject('Оплатите регистрацию');
		});*/

		return Redirect::to("/user/structure");
	}

	public function getNews()
	{
		$admin = Auth::admin()->get();

		$posts = Post::orderBy('created_at', 'DESC')->get();

		return view('admin.news',
			[
				'user' => $admin,
				'posts' => $posts
			]
		);
	}

	public function getContacts()
	{
		$admin = Auth::admin()->get();

		return view("shared.contacts", ['user' => $admin, 'extends' => 'admin']);
	}

	public function getEditProfile()
	{
		$admin = Auth::admin()->get();

		return view("admin.edit_profile", ['user' => $admin]);
	}

	public function postEditProfile()
	{
		$admin = Auth::admin()->get();
		$admin->surname = Request::input("surname");
		$admin->name = Request::input("name");
		$admin->email = Request::input("email");

		$admin->save();

		return Redirect::back();
	}

	public function postChangePassword()
	{
		$admin = Auth::admin()->get();
		$old_password = Request::input("old_password");
		$new_password = Request::input("new_password");
		if(\Hash::check($old_password, $admin->password)){
			$admin->password = \Hash::make($new_password);
			$admin->save();
		}
		else
		{
			return Redirect::back()->with("error_edit","Неправильный старый пароль");
		}

		return Redirect::back()->with("error_edit","Вы успешно поменяли пароль");
	}

	public function getEditNews($id)
	{
		if (is_numeric($id))
		{
			$admin = Auth::admin()->get();

			$post = Post::find($id);

			return view("admin.edit_news", ['user' => $admin, 'posts' => $post]);
		}
		else
		{
			return redirect()->back();
		}
	}

	public function postEditNews()
	{
		$id = Request::input("post_id");

		if(is_numeric($id))
		{
			$posts = Post::find($id);
		}
		else
		{
			return Redirect::back();
		}
		if (Request::hasFile('image_name'))
		{
			$dir = '/post_images/';
			$destinationPath = \Config::get('app.img_path') . $dir;

			$file = Request::file('image_name');
			$filename = str_random(40);

			while(file_exists($destinationPath.$filename.'.jpg'))
				$filename = str_random(40);

			$upload_success = $file->move($destinationPath, $filename. '.jpg');

			if ($upload_success)
			{
				try
				{
					/*$img = \Image::make($destinationPath.$filename. '.jpg');
					$img->resize(400, null, function ($constraint)
						{
							$constraint->aspectRatio();
							$constraint->upsize();
						}
					);*/
					//$img->save($destinationPath.$filename.'.jpg');
					$posts->image = \Config::get('app.img_url') . $dir . $filename.'.jpg';
				}
				catch(Exception $e)
				{
					//Log::alert("Invalid photo [".$destinationPath.$filename."] image type [".$file->getClientOriginalExtension()."] by company [".$company->id."].");
				}

			}
		}
		$posts->title = Request::input("title");
		$posts->description = Request::input("description");
		$posts->post = Request::input("news");

		$posts->save();

		return Redirect::back();
	}

	public function getAddNews()
	{
		$admin = Auth::admin()->get();

		return view("admin.add_news", ['user' => $admin]);
	}

	public function postAddNews()
	{
		$posts = new Post;
		if (Request::hasFile('image_name'))
		{
			$dir = '/post_images/';
			$destinationPath = \Config::get('app.img_path') . $dir;

			$file = Request::file('image_name');
			$filename = str_random(40);

			while(file_exists($destinationPath.$filename.'.jpg'))
				$filename = str_random(40);

			$upload_success = $file->move($destinationPath, $filename. '.jpg');

			if ($upload_success)
			{
				try
				{
					/*$img = \Image::make($destinationPath.$filename. '.jpg');
					$img->resize(400, null, function ($constraint)
						{
							$constraint->aspectRatio();
							$constraint->upsize();
						}
					);*/
					//$img->save($destinationPath.$filename.'.jpg');
					$posts->image = \Config::get('app.img_url') . $dir . $filename.'.jpg';
				}
				catch(Exception $e)
				{
					//Log::alert("Invalid photo [".$destinationPath.$filename."] image type [".$file->getClientOriginalExtension()."] by company [".$company->id."].");
				}

			}

			$posts->title = Request::input("title");
			$posts->description = Request::input("description");
			$posts->post = Request::input("news");
			$posts->save();

		}

		return Redirect::to("/admin/add-news");
	}

	public function getRemoveNews($id)
	{
		$post = Post::find($id);
		$post->delete();

		return redirect()->back();
	}

	public function getUsersList()
	{
		$admin = Auth::admin()->get();

		$list_user = ClubUser::orderBy('active', 'ASC')->orderBy('created_at', 'DESC')->paginate(10);

		return view("admin.users_list", ['user' => $admin, "users_list"=> $list_user]);
	}

    public function getHouseList()
    {
        $admin = Auth::admin()->get();

        $list_user = HouseUser::orderBy('active', 'ASC')->orderBy('created_at', 'DESC')->paginate(10);

        return view("admin.house_users_list", ['user' => $admin, "users_list"=> $list_user]);
    }

    public function getSeeUser($id)
    {
        $user = ClubUser::find($id);

        return view('admin.team', compact('user'));
    }

    public function getSeeHouseUser($id)
    {
        $user = HouseUser::find($id);

        return view('admin.house_team', compact('user'));
    }

	public function getGeneratePin()
	{
		$id = Request::input('id');

		$pin = str_random(4);

		$validation = Validator::make(
			['PIN' => $pin],
			['PIN' => 'unique:club_users']
		);

		while ($validation->fails())
		{
			$pin = str_random(4);

			$validation = Validator::make(
				['PIN' => $pin],
				['PIN' => 'unique:users']
			);
		}

		$user = ClubUser::find($id);
		$user->PIN = $pin;
        //$user->active = 1;
        $user->hasPayed = NULL;
		$user->save();
		$email = $user->email;

		/*Mail::send('emails.given_pin', ['user' => $user, 'pin' => $pin], function($message) use ($email) {
			$message->to($email)->subject('Выдача пин кода на сайте luxurylife.us');
		});*/

		echo $pin;

		exit;
	}

    public function getHouseGeneratePin()
    {
        $id = Request::input('id');

        $pin = str_random(4);

        $validation = Validator::make(
            ['PIN' => $pin],
            ['PIN' => 'unique:house_users']
        );

        while ($validation->fails())
        {
            $pin = str_random(4);

            $validation = Validator::make(
                ['PIN' => $pin],
                ['PIN' => 'unique:house_users']
            );
        }

        $user = HouseUser::find($id);
        $user->PIN = $pin;
        //$user->active = 1;
        $user->save();
        $email = $user->email;

        /*Mail::send('emails.given_pin', ['user' => $user, 'pin' => $pin], function($message) use ($email) {
            $message->to($email)->subject('Выдача пин кода на сайте luxurylife.us');
        });*/

        echo $pin;

        exit;
    }

	public function getLogin()
	{
		return view('admin.login');
	}

	public function postLogin()
	{
		$login = Request::input('login');
		$password = Request::input('password');

		if (Auth::admin()->attempt(['login' => $login, 'password' => $password]))
		{
			return redirect()->to('/admin/profile');
		}
		else
		{
			return redirect()->back()->withInput()->with('error-message', 'Неправильные данные для входа');
		}
	}

	public function getLogout()
	{
		Auth::admin()->logout();
		return redirect()->to('/');
	}

	public function postChangeAvatar()
	{
		if (Request::hasFile('avatar'))
		{
			$user = Auth::admin()->get();
			$dir = '/users/avatars/';
			$destinationPath = \Config::get('app.img_path') . $dir;

			if( ! (strpos($user->avatar, 'user.png') || $user->avatar == "")) {
				$arr = explode('/',$user->avatar);
				unlink(public_path() ."/uploads". $dir . $arr[6]);
			}

			$file = Request::file('avatar');
			$filename = str_random(40);

			while(file_exists($destinationPath.$filename.'.jpg'))
				$filename = str_random(40);

			$upload_success = $file->move($destinationPath, $filename. '.jpg');

			if ($upload_success)
			{
				try
				{
					/*$img = \Image::make($destinationPath.$filename. '.jpg');
					$img->resize(400, null, function ($constraint)
						{
							$constraint->aspectRatio();
							$constraint->upsize();
						}
					);
					$img->save($destinationPath.$filename.'.jpg');*/

					$user->avatar = \Config::get('app.img_url') . $dir . $filename.'.jpg';
					$user->save();
				}
				catch(Exception $e)
				{
					//Log::alert("Invalid photo [".$destinationPath.$filename."] image type [".$file->getClientOriginalExtension()."] by company [".$company->id."].");
				}

			}
		}
	}

	public function getOneStructure()
 	{
		$id = Request::input("id");
 		if (is_numeric($id))
 		{
 			$user = ClubUser::find($id);

 			$structure = ClubUser::where("parent_id", $id)->where("active", 1)->get();
 			return view('user.subscriber',
 				['structure'=> $structure,
 				 'user'=>$user
 				]);
 		}
		exit;
 	}

 	public function getSearch()
 	{
 		$search = Request::input("search");

 		$list_user = ClubUser::where("login", 'LIKE', "%".$search."%")
				->orWhere("name", 'LIKE',  "%".$search."%")
				->orWhere("id", 'LIKE',  "%".$search."%")->get();;

		return view("admin.search_user_list", ["users_list" => $list_user]);


 		exit;
 	}

    public function getHouseSearch()
    {
        $search = Request::input("search");

        $list_user = HouseUser::where("login", 'LIKE', "%".$search."%")
            ->orWhere("name", 'LIKE',  "%".$search."%")
            ->orWhere("id", 'LIKE',  "%".$search."%")->get();;

        return view("admin.house_search_user_list", ["users_list" => $list_user]);


        exit;
    }

 	public function getShow($id)
 	{
 		if(is_numeric($id))
 		{
 			$post = Post::find($id);

	 		$user = Auth::admin()->get();
	 		return view("admin.show", ['post' => $post, 'user' => $user]);
 		}
 		else
 		{
 			App::abort(404);
 		}

 	}

 	public function getReset()
 	{
 		return view("pre-auth.password_reset", ['entity' => 'admin']);
 	}

 	public function postReset()
 	{
 		$email = Request::input("email");


		$email_exist = Admin::where('email', $email)->first();
		if($email_exist)
		{
			$token = str_random(30);

			$validation = Validator::make(
				['token' => $token],
				['token' => 'unique:password_resets']
			);

			while ($validation->fails())
			{
				$token = str_random(30);

				$validation = Validator::make(
					['token' => $token],
					['token' => 'unique:password_resets']
				);
			}
			$password_resets = new PasswordReset;
			$password_resets->email = $email;
			$password_resets->token = $token;
			$password_resets->save();
			$url = \Config::get('app.url');
			$link = $url.'/admin/make-password?token='.$token;
			Mail::send('emails.password_remind', ['user' => $email_exist, 'link' => $link], function($message) use ($email) {
				$message->to($email)->subject('Восстановление пароля на сайте luxurylife.us');
			});
		}
		else
		{
			return Redirect::back()->with("error", "Данный email не найден");
		}
		return Redirect::to("after-reset");
 	}

 	public function getMakePassword()
	{
		if(Request::has('token')){

			$pass_reset = PasswordReset::where('token', Request::input("token"))->first();
			$email = null;
			$token_send = Request::input("token");
			if(!$pass_reset or $pass_reset->active == 0)
			{
				\Session::set('error', "Неправильная ссылка");
			}
			else
			{
				$time = $pass_reset->created_at;

				$hour = date("H", strtotime($time));
				$minute = date("i", strtotime($time)) + $hour*60;
				$date = date("Y:m:d", strtotime($time));


				$now_date = date("Y:m:d");
				$now_minute = date('i')+ date('H')*60;
				$email = $pass_reset->email;

				if($now_date != $date or $now_minute-$minute > 60)
				{
					\Session::set('error', "Это ссылка устарела");

				}
			}
		}else{
			return Redirect::to("/admin/reset");
		}


		\Session::set('reset_email', $email);
		\Session::set('token_send', $token_send);
		return view("pre-auth.reset_link",['entity' => 'admin'] );
	}

	public function postMakePassword()
	{
		if(\Session::has("reset_email") and \Session::has("token_send"))
		{
			$email = \Session::get("reset_email");
			$token_send = \Session::get("token_send");
		}
		else
		{
			return Redirect::back()->with("error", "Неправильная ссылка");
		}

		$password = Request::input("password");
		$adminEmail = Admin::where("email",$email)->first();
		$clubEmail = ClubUser::where("email",$email)->first();
		$homeEmail = HouseUser::where("email",$email)->first();

		if($adminEmail != null )
		{
			$adminEmail->password = \Hash::make($password);
			$adminEmail->save();
		}
		else if($clubEmail != null)
		{
			$clubEmail->password = \Hash::make($password);
			$clubEmail->save();
		}
		else if($homeEmail != null)
		{
			$homeEmail->password = \Hash::make($password);
			$homeEmail->save();
		}



		\DB::table('password_resets')
            ->where('token', $token_send)
            ->update(['active' => 0]);


		return Redirect::back()->with("success", "Вы успешно поменяли пароль");
	}

	public function getRequests()
	{
		$admin = Auth::admin()->get();
		$requests = UserRequest::orderBy('created_at', 'DESC')->paginate(10);

		return view('admin.requests_list',
			[
				'user' => $admin,
				'requests' => $requests,
			]
		);
	}

}
