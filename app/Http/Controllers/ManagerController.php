<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Auth, App, Validator,  Config ,Input ,Hash , App\User,App\Tag,Redirect, Request;

class ManagerController extends Controller {

	public function getIndex()
	{
		return view("manager.news");
	}

	public function getProfile()
	{	
		return view("manager.profile");
	}

	public function getStructure()
	{
		return view("manager.structure");
	}
	public function getRules()
	{
		return view("manager.rules");
	}

	public function getMarketing()
	{
		return view("manager.marketing");
	}

	public function getSignup()
	{
		return view("manager.signup");
	}

	public function getNews()
	{	
		return view("manager.news");
	}

	public function getContacts()
	{
		return view("manager.contacts");
	}

	public function getEditProfile()
	{	
		return view("manager.edit_profile");
	}

	public function postEditProfile()
	{

		$users = User::find(Auth::manager()->get()->id);

		if(Request::input("old_password")!="")
		{
		  	if (Hash::check(Request::input("old_password"), Auth::manager()->get()->password))
		  	{
		  		$users->password = Hash::make(Request::input("new_password"));
		  	}
		  	else
		  	{
		  		return Redirect::to("/manager/edit-profile")->with("message","Пароль неверный");
		  	}
		}

		$users->name = Request::input("name");
		$users->surname = Request::input("surname");
		$users->patronymic = Request::input("patronymic");
		$users->phone = Request::input("phone");
		$users->bank_account = Request::input("bank_account");
		$users->bank_name = Request::input("bank_name");
		$users->bik = Request::input("bik");
		$users->card_id = Request::input("card_id");
		$users->birth_date = date(Request::input("birth_date"));
		$users->address = Request::input("address");
		$users->save();

		return Redirect::to("/manager/edit-profile");
	}

	public function postChangeAvatar()
	{
		dd(Request::all());
	}

}
