<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class HouseUser extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword, SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'house_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'surname', 'patronymic', 'email'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	// return all user generated links
	public function links()
	{
		return $this->hasMany('App\UserLink');
	}

	// get children where active = 1
	public function children()
	{
		return $this->hasMany('App\HouseUser', 'parent_id', 'id')->where('active', 1)->orderBy('activated_at', 'ASC');
	}

	public function childrenEuroCount($i)
	{
		return $this->hasMany('App\HouseUser','parent_id', 'id')->where("cycle",">=", $i)->where('active', 1)->orderBy('activated_at', 'ASC');
	}

	public function parent()
	{
		return $this->belongsTo('App\HouseUser', 'parent_id', 'id');
	}

	public function encouraged()
	{
		return $this->hasMany('App\HouseUser', 'encourager_id', 'id')->where('active', 1);
	}

	public function encourager()
	{
		return $this->belongsTo('App\HouseUser', 'encourager_id', 'id');
	}

	public function sponsor($id)
	{
		$user = $this->find($id);
		return $user;
		//return $this->BelongsTo('App\ClubUser', 'old_parent_id', 'id')->where('active', 1)->where("old_parent_id", "<>", -1);
	}

	// при первой регистрации мы должны найти ему родителя.
	// принцип слева направо, находим того, у кого не 2 подъюзера
	public static function getFairPosition()
	{
		// check if users exist, if not - return null, so that registered user will be first
		$users = static::all();
		if ( ! $users->count() )
        {
            return null;
        }

		/*
			Ищем на какой уровень записать нового пользователя.
			Если 2^i != кол-ву пользователей на i-уровне, то прерываем цикл и записываем пользователя
			на этот уровень
		*/

		$sloy = 0;

		while (true)
		{
			$level_count = static::where('level', $sloy)->where('active' , 1)->count();
			if($level_count != pow(2, $sloy))
			{
				$sloy--;
				break;
			}
			$sloy++;
		}

		$users = static::where('level', $sloy)->get();

		foreach ($users as $key => $user)
		{
			if ($user->children->count() < 2)
			{
				return $user;
			}
		}
	}

	public static function getFairChild($global_parent)
	{
		if($global_parent->children->count() < 2)
		{
			return $global_parent;
		}

		$parent = $global_parent->children;

		while (true)
		{
			$potencial_child = $parent;
			$index = 0;
			foreach ($potencial_child as $key => $child)
			{
				if($child->children->count() < 2)
				{
					return $child;
				}
				else
				{
					foreach ($child->children as $key1 => $value) {
						$parent[$index++] = $value;
					}
				}
			}
		}
	}

	/*Проверка перехода parent на следующий уровень при регистрации его детей*/
	public function getCycleChild()
	{
		$parent = $this->find($this->parent_id);

		if(!$parent || $parent->children->count() < 2)
		{
			return null;
		}

		$possible_parent = $this->find($parent->parent_id);

		if(!$possible_parent)
		{
			return null;
		}

		if ($possible_parent->children->count() < 2)
		{
			return null;
		}

		foreach ($possible_parent->children as $key => $value)
		{
			if($value->children->count() < 2)
			{
				return null;
			}
		}

		return $possible_parent;

	}

    /****
     *  Проверка родителя 0 уровня на количество поддетей 4 уровня
     *  (21 в общем)
     *  и переход родителя в последующий цикл
     *
     *  Использовано в Seeds и ClubController( метод postRegister )
     */
        // public static function getTwentyOne($id)
        // {
        //     // свежий зарегестрированный пользователь
        //     $new_one = static::find($id);
        //
        //     // количество пользователей четвертого уровня
        //     $forth_count = static::where('level', 4)->count();
        //
        //     // если пользователей четвертого уровня 21,
        //     // то перевести пользователя нулевого уровня
        //     // (относительно них) на новый цикл
        //     if ($forth_count >= 21)
        //     {
        //         // Находим пользователя на уровень выше (3 уровень)
        //         $parent1 = static::find($new_one->parent_id);
        //         if ($parent1)
        //         {
        //             // Находим пользователя на уровень выше (2 уровень)
        //             $parent2 = static::find($parent1->parent_id);
        //             if ($parent2)
        //             {
        //                 // Находим пользователя на уровень выше (1 уровень)
        //                 $parent3 = static::find($parent2->parent_id);
        //                 if ($parent3)
        //                 {
        //                     // Находим пользователя на уровень выше (0 уровень)
        //                     // Уже почти ...
        //                     $parent4 = static::find($parent3->parent_id);
        //                     if ($parent4)
        //                     {
        //                         // Вот оно
        //                         // Тут переход в новый цикл (2)
        //                         $parent4->cycle = 2;
        //                         $parent4->save();
        //                     }
        //                 }
        //             }
        //         }
        //
        //     }
        //
        // }


}
