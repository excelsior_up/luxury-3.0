<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

// Для переноса
use App\ClubUser2;

class ClubUser extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'club_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'surname', 'patronymic', 'email'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	// return all user generated links
	public function links()
	{
		return $this->hasMany('App\UserLink');
	}

	// get children where active = 1
	public function children()
	{
		return $this->hasMany('App\ClubUser', 'parent_id', 'id')->where('active', 1)->orderBy('activated_at', 'ASC');
	}

	public function childrenEuroCount($i)
	{
		return $this->hasMany('App\ClubUser','parent_id', 'id')->where("cycle",">=", $i)->where('active', 1)->orderBy('activated_at', 'ASC');
	}

	public function parent()
	{
		return $this->belongsTo('App\ClubUser', 'parent_id', 'id');
	}

	public function encouraged()
	{
		return $this->hasMany('App\ClubUser', 'encourager_id', 'id')->where('active', 1);
	}

	public function encourager()
	{
		return $this->belongsTo('App\ClubUser', 'encourager_id', 'id');
	}

	public function sponsor($id)
	{
		$user = $this->find($id);
		return $user;
		//return $this->BelongsTo('App\ClubUser', 'old_parent_id', 'id')->where('active', 1)->where("old_parent_id", "<>", -1);
	}

	// при первой регистрации мы должны найти ему родителя.
	// принцип слева направо, находим того, у кого не 3 подъюзера
	public static function getFairPosition()
	{
		// check if users exist, if not - return null, so that registered user will be first
		$users = static::all();
		if ( ! $users->count() )
        {
            return null;
        }

		/*
			Ищем на какой уровень записать нового пользователя.
			Если 3^i != кол-ву пользователей на i-уровне, то прерываем цикл и записываем пользователя
			на этот уровень
		*/

		$sloy = 0;

		while (true)
		{
			$level_count = static::where('level', $sloy)->where('active' , 1)->count();
			if($level_count != pow(3, $sloy))
			{
				$sloy--;
				break;
			}
			$sloy++;
		}

		$users = static::where('level', $sloy)->get();

		foreach ($users as $key => $user)
		{
			if ($user->children->count() < 3)
			{
				return $user;
			}
		}
	}

	public static function getFairChild($global_parent)
	{
		if($global_parent->children->count() < 3)
		{
			return $global_parent;
		}

		$parent = $global_parent->children;

		while (true)
		{
			$potencial_child = $parent;
			$index = 0;
			foreach ($potencial_child as $key => $child)
			{
				if($child->children->count() < 3)
				{
					return $child;
				}
				else
				{
					foreach ($child->children as $key1 => $value) {
						$parent[$index++] = $value;
					}
				}
			}
		}
	}

	/*Проверка перехода parent на следующий уровень при регистрации его детей*/
	public function getCycleChild()
	{
		$parent = $this->find($this->parent_id);

		if(!$parent || $parent->children->count() < 3)
		{
			return null;
		}

		$possible_parent = $this->find($parent->parent_id);

		if(!$possible_parent)
		{
			return null;
		}

		if ($possible_parent->children->count() < 3)
		{
			return null;
		}

		foreach ($possible_parent->children as $key => $value)
		{
			if($value->children->count() < 3)
			{
				return null;
			}
		}

		return $possible_parent;

	}

    /****
     *  Проверка родителя 0 уровня на количество поддетей 4 уровня
     *  (21 в общем)
     *  и переход родителя в последующий цикл
     *
     *  Использовано в Seeds, ClubController( метод postRegister )
     *  и в MainController ( метод postSignup )
     */
    public static function getTwentyOne($id)
    {
        // свежий зарегестрированный пользователь
        $new_one = static::find($id);
        $level = $new_one->level;

        // количество пользователей четвертого уровня
        if ($level <= 4)
        {
            $forth_count = static::where('level', 4)->count();
        }
        else
        {
            // если же новый пользователь имеет >5 уровень
            // то переход будет относительно этого уровня
            // (для 4 -> 0, а для 5 -> 1 и т.д.)
            $forth_count = static::where('level', $level)->count();
        }

        // если пользователей четвертого уровня 21,
        // то перевести пользователя нулевого уровня
        // (относительно них) на новый цикл
        if ($forth_count >= 21)
        {
            // Находим пользователя на уровень выше (3 уровень)
            $parent1 = static::find($new_one->parent_id);
            if ($parent1)
            {
                // Находим пользователя на уровень выше (2 уровень)
                $parent2 = static::find($parent1->parent_id);
                if ($parent2)
                {
                    // Находим пользователя на уровень выше (1 уровень)
                    $parent3 = static::find($parent2->parent_id);
                    if ($parent3)
                    {
                        // Находим пользователя на уровень выше (0 уровень)
                        // Уже почти ...
                        $parent4 = static::find($parent3->parent_id);
                        if ($parent4)
                        {
                            // Вот оно
                            // Тут переход в новый цикл (2)

                            $parent4->cycle = 2;
                            $parent4->save();

                            $new_in_second = ClubUser2::where('surname', $parent4->surname)
                                ->where('name', $parent4->name)
                                ->where('patronymic', $parent4->patronymic)
                                ->where('email', $parent4->email)
                                ->first();

                            if ($new_in_second != null)
                            {
                                return true;
                            } else {
                                $new_in_second = new ClubUser2;

                                $new_in_second->encourager_id = $parent4->encourager_id;
                                $new_in_second->parent_id = $parent4->parent_id;
                                $new_in_second->cycle = $parent4->cycle;
                                $new_in_second->level = $parent4->level;
                                $new_in_second->position = $parent4->position;

                                $new_in_second->PIN = $parent4->PIN;
                                $new_in_second->login = $parent4->login;

                                $new_in_second->name = $parent4->name;
                        		$new_in_second->surname = $parent4->surname;
                        		$new_in_second->patronymic = $parent4->patronymic;
                        		$new_in_second->phone = $parent4->phone;
                        		$new_in_second->bank_account = $parent4->bank_account;
                        		$new_in_second->bank_name = $parent4->bank_name;
                        		$new_in_second->bik = $parent4->bik;
                        		$new_in_second->card_id = $parent4->card_id;
                        		$new_in_second->email = $parent4->email;

                        		$new_in_second->birth_date = $parent4->birth_date;
                        		$new_in_second->address = $parent4->address;
                        		$new_in_second->save();
                            }

                        }
                    }
                }
            }

        }

    }
}
